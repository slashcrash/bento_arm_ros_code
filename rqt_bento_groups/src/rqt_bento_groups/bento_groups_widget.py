import rospy
import rospkg
import os
from bento_controller.msg import *
from bento_controller.srv import *
from python_qt_binding import loadUi
from .joint_groups_widget import JointGroupsWidget
from python_qt_binding.QtGui import QWidget, QVBoxLayout

class BentoGroupsWidget(QWidget):
    def __init__(self, context):
        super(BentoGroupsWidget, self).__init__()
        
        rp = rospkg.RosPack()
        ui_file = os.path.join(rp.get_path('rqt_bento_groups'), 'resource', 'bento_groups.ui')
        loadUi(ui_file, self)
        
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        
        self.setObjectName('BentoGroupsWidget')

        self._load_joint_groups()
        
    def _load_joint_groups(self):
        joint_groups = JointGroupsWidget()
        self.layout.addWidget(joint_groups)
            