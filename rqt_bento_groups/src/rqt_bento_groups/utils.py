#!/usr/bin/env python

def get_display_name_for_joint_idx(bento_enumeration, joint_idx):
    assert joint_idx <= len(bento_enumeration.joints)
    return bento_enumeration.joints[joint_idx].name

def get_display_name_for_joint_id(bento_enumeration, joint_id):
    idx = get_joint_idx_from_joint_id(bento_enumeration, joint_id)
    return get_display_name_for_joint_idx(bento_enumeration, idx)

def get_joint_id_from_joint_idx(bento_enumeration, joint_idx):
    assert joint_idx <= len(bento_enumeration.joints)
    return bento_enumeration.joints[joint_idx].id

def get_joint_idx_from_joint_id(bento_enumeration, joint_id):
    return next((idx for idx in range(len(bento_enumeration.joints)) if
                 bento_enumeration.joints[idx].id == joint_id), None)
