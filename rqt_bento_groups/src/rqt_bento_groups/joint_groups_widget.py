#!/usr/bin/env python

import rospy
from python_qt_binding.QtGui import *
from joint_group_widget import JointGroupWidget

from bento_controller.msg import *
from bento_controller.srv import *

import utils

class JointGroupsWidget(QWidget):
    
    def __init__(self):
        super(JointGroupsWidget, self).__init__()

        self.bento_enumerate_proxy = rospy.ServiceProxy("/bento/enumerate", bento_controller.srv.Enumerate)        
        self.bento_enumeration = self.bento_enumerate_proxy()
        self.setObjectName('JointGroupsWidget')
        
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        
        self._load_joint_groups()
        
        rospy.Subscriber("/bento/selected_joint", JointSelection, self._selected_joint_callback)

    def _load_joint_groups(self):
        
        self.joint_summary = JointSummaryWidget(self.bento_enumeration.joint_groups, self.bento_enumeration)
        self.layout.addWidget(self.joint_summary)
        
        self.joint_group_tab = QTabWidget()
        self.layout.addWidget(self.joint_group_tab)
        
        for idx, joint_group in enumerate(self.bento_enumeration.joint_groups):
            joint_group_widget = JointGroupWidget(joint_group, self.bento_enumeration.joints)
            self.joint_group_tab.addTab(joint_group_widget, "%d" % (idx + 1))
            
    def _selected_joint_callback(self, msg):
        self.joint_summary.update_selection(msg.joint_group, msg.joint_id)
        
        
    def add_joint_group(self):
        pass
        
class JointSummaryWidget(QWidget):
    def __init__(self, joint_groups, bento_enumeration):
        super(JointSummaryWidget, self).__init__()
        self.joint_groups_enumeration = joint_groups
        self.bento_enumeration = bento_enumeration
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.joint_groups = []
        
        font = QFont("Arial", 28, QFont.Bold)
        
        
        for idx, joint_group in enumerate(joint_groups):
            layout = QHBoxLayout()
            self.joint_groups.append(layout)

            # Joint group
            label = QLabel("%d" % (idx + 1))
            label.setFont(font)
            layout.addWidget(label)
            
            # Selected Joint
            label = QLabel(self.get_display_name_for_joint(joint_group.selected_joint_id))
            label.setFont(font)
            layout.addWidget(label)
            self.layout.addLayout(layout)
                
    def get_display_name_for_joint(self, joint_id):
        return utils.get_display_name_for_joint_id(self.bento_enumeration, joint_id)
        
    def update_selection(self, joint_group_idx, joint_id):
        layout = self.joint_groups[joint_group_idx]
        label = layout.itemAt(1).widget()
        label.setText(self.get_display_name_for_joint(joint_id))
                
            
            
            
            
    