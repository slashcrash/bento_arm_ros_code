#!/usr/bin/env python

import rospy
import rospkg
import os
from python_qt_binding import loadUi
from python_qt_binding import QtCore
from python_qt_binding.QtCore import Qt
from python_qt_binding.QtGui import *

from bento_controller.msg import *
from bento_controller.srv import *

def clearLayout(layout):
    """
    Copied from the internets
    """
    while layout.count():
        child = layout.takeAt(0)
        if child.widget() is not None:
            child.widget().deleteLater()
        elif child.layout() is not None:
            clearLayout(child.layout())

class JointGroupWidget(QWidget):
    # Weird, does not work if instance variable
    button_press = QtCore.pyqtSignal(int)
    config_signal = QtCore.Signal(JointGroup)
 
    def __init__(self, joint_group, joints):
        super(JointGroupWidget, self).__init__()
        
        self.joints = {}
        for joint in joints:
            self.joints[joint.id] = joint
            
        rp = rospkg.RosPack()
        ui_file = os.path.join(rp.get_path('rqt_bento_groups'), 'resource', 'joint_group.ui')
        loadUi(ui_file, self)
        self.setObjectName('JointGroupWidget')
        
        self.toggle_forward_button.setIcon(QIcon.fromTheme("go-next"))
        self.toggle_backward_button.setIcon(QIcon.fromTheme("go-previous"))

        self.toggle_service = rospy.ServiceProxy("/bento/toggle_group", ToggleGroup)
        self.select_joint_service = rospy.ServiceProxy("/bento/select_joint", SelectJoint)
        
        self.toggle_forward_button.clicked[bool].connect(self._handle_toggle_forward_press)
        self.toggle_backward_button.clicked[bool].connect(self._handle_toggle_backward_press)
        
        self.selected_joint_label.setFont(QFont("Arial", 28, QFont.Bold))
        
        rospy.Subscriber("/bento/selected_joint", JointSelection, self._selected_joint_callback)
        rospy.Subscriber("/bento/joint_group_update", JointGroup, self._joint_group_update_callback)
        
        self.button_press.connect(self._handle_joint_selection)
        
        def configure_slot(msg):
            self._configure(msg)
            
        self.config_signal.connect(configure_slot)

        self._configure(joint_group)
        
    def _configure(self, config):
        self.joint_group = config
            
        clearLayout(self.joint_list)
            
        def remitter(idx):
            def  emitter():
                self.button_press.emit(idx)
            return emitter
    
        for idx, joint in enumerate(self.joint_group.joints):
            button = QPushButton("%s" % self.get_display_name_for_joint(joint.joint_id))
            button.clicked[bool].connect(remitter(idx))
            self.joint_list.addWidget(button)
            
        self._update_active_joint_display()
        
    def _joint_group_update_callback(self, msg):
        if(msg.group_idx == self.joint_group.group_idx):
            self.config_signal.emit(msg)
        
    def _handle_joint_selection(self, idx):
        resp = self.select_joint_service(group_idx = self.joint_group.group_idx, joint_idx = idx)
        self.joint_group.selected_joint_id = resp.joint_id
        self._update_active_joint_display()
    
    def _handle_toggle_forward_press(self):
        self._handle_toggle_press(False)
        
    def _handle_toggle_backward_press(self):
        self._handle_toggle_press(True)
        
    def _handle_toggle_press(self, backward):
        resp = self.toggle_service(group_idx = self.joint_group.group_idx, backward = backward)
        self.joint_group.selected_joint_id = resp.joint_id
        self._update_active_joint_display()  
        
    def _selected_joint_callback(self, msg):
        if msg.joint_group == self.joint_group.group_idx:
            self.joint_group.selected_joint_id = msg.joint_id
            self._update_active_joint_display()
        
    def _update_active_joint_display(self):
        self.selected_joint_label.setText("%s" %
                                          self.get_display_name_for_joint(self.joint_group.selected_joint_id))
            
    def get_joint_by_id(self, joint_id):
        return self.joints[joint_id] if joint_id in self.joints else None
    
    def get_display_name_for_joint(self, joint_id):
        joint = self.get_joint_by_id(joint_id)
        return joint.name if joint else str(joint_id)
        
            
            
    
        
        
        