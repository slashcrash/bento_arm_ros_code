#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
from dynamixel_msgs.msg import *
from single_joint_controller import SingleJointController

class MultiModeDynamixelController(SingleJointController):
    """
    Previously in order to have both velocity and position control over a single joint a separate controller
    of each type was created for each joint. This caused some problems as the two weren't aware of one another.
    
    I'm still not overly happy with this implementation which requires calling SetMode to set the mode. It
    would be better to simply call the appropriate topic or have
    """
    
    def __init__(self, dxl_io, controller_namespace, port_namespace):
        SingleJointController.__init__(self, dxl_io, controller_namespace, port_namespace)
        
        self.last_diff = 0.0
        self.hold_pos = None
        
        # I think this should actually be performed in the main dynamixel code
        self.dxl_io.set_angle_limits(self.motor_id, self.min_angle_raw, self.max_angle_raw)        

    def start(self):
        self.running = True
        self.velocity_sub = rospy.Subscriber(self.controller_namespace + '/command/velocity', Float64, self.process_velocity_command)
        self.position_sub = rospy.Subscriber(self.controller_namespace + '/command/position', Float64, self.process_position_command)        
        self.joint_state_pub = rospy.Publisher(self.controller_namespace + '/state', JointState, queue_size=1)
        self.motor_states_sub = rospy.Subscriber('motor_states/%s' % self.port_namespace, MotorStateList, self.process_motor_states)        
        
    def stop(self):
        self.running = False
        self.joint_state_pub.unregister()
        self.motor_states_sub.unregister()
        self.velocity_sub.unregister()
        self.position_sub.unregister()
        self.speed_service.shutdown('normal shutdown')
        self.torque_service.shutdown('normal shutdown')
        self.compliance_slope_service.shutdown('normal shutdown')        
        
    def process_torque_command(self, msg):
        raise NotImplementedError
    
    def process_torque_enable(self, req):
        self.hold_pos = None
        return SingleJointController.process_torque_enable(self, req)
    
    def process_motor_states(self, state_list):
        pos_1 = self.joint_state.current_pos
        SingleJointController.process_motor_states(self, state_list)
        self.last_diff = self.joint_state.current_pos - pos_1
        
    def process_position_command(self, msg):
        self.hold_pos = None
        angle = msg.data
        mcv = (self.motor_id, self.pos_rad_to_raw(angle))
        self.dxl_io.set_multi_position([mcv])
        
    def process_velocity_command(self, msg):
        speed = msg.data
        angle = 0
        
        if(speed == 0):
            if self.hold_pos is None:
                self.hold_pos = self.joint_state.current_pos + self.last_diff
            mcv = (self.motor_id, self.pos_rad_to_raw(self.hold_pos))
        else:
            self.hold_pos = None
            if(speed > 0):
                angle = self.max_angle
            elif (speed < 0):
                angle = self.min_angle

            self.set_speed(abs(speed))
            mcv = (self.motor_id, self.pos_rad_to_raw(angle))
        
        self.dxl_io.set_multi_position([mcv])