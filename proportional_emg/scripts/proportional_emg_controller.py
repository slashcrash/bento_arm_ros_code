#!/usr/bin/env python
import rospy

from bento_controller.msg import *
from bento_controller.srv import *
from adc_msgs.msg import ADCSignal
from std_msgs.msg import String, Float32MultiArray, Header
from proportional_emg.srv import *
from proportional_emg.msg import *
from ros_utils.timed import timed

import numpy as np

class ProportionalEMGController(object):
  """
  A ProportionalEMG Controller for use with Bento
  """
  
  ros_prefix = "proportional_emg"
  
  # TODO: should not be using static members in this way.
  
  # Whether we are currently toggling, use this to prevent sending toggle commands repeatedly.
  toggle_states = []
  
  # publishers used for publishing information about a channel configuration change
  channel_update_publishers = {}

  def __init__(self):    
    self.config = rospy.get_param(self.ros_prefix)
    self.paused = True
    
    # initializing toggle groups
    if "toggle" in self.config:
      for idx, toggle in enumerate(self.config["toggle"]):
        self.toggle_states.append(False)
      
    # setup publishers for channels
    for idx, channel in enumerate(self.config["channels"]):
      self.channel_update_publishers[channel["channel"]] = rospy.Publisher("/%s/channels/%d" % (self.ros_prefix, channel["channel"]), Channel, queue_size=10)
    
    # creating a buffer for reading the DAQ values
    self.buffer = np.zeros((self.config["window_length"], len(self.config["channels"])))
    
    # subscribe to ADCSignals
    rospy.Subscriber("/adc", ADCSignal, self._emg_callback, queue_size=1)

    # publishers
    self.publisher = rospy.Publisher("/bento/group/command", BentoGroupCommand, queue_size=1)
    self.velocity_publisher = rospy.Publisher("/%s/velocities" % self.ros_prefix, Float32MultiArray, queue_size=10)
    self.mavs_publisher = rospy.Publisher("/%s/mavs" % self.ros_prefix, Float32MultiArray, queue_size=10)
    self.state_publisher = rospy.Publisher("/%s/state" % self.ros_prefix, State, queue_size=10)
    
    # services
    rospy.Service('/%s/enumerate' % self.ros_prefix, Enumerate, self._enumerate)
    rospy.Service('/%s/set_channel_parameters' % self.ros_prefix, SetChannelParameters, self._set_channel_parameters)
    rospy.Service('/%s/pause' % self.ros_prefix, Pause, self._pause)
    rospy.Service('/%s/get_state' % self.ros_prefix, GetState, self._get_state)
    
    # proxies
    self.toggle_proxy = rospy.ServiceProxy('/bento/toggle_group', ToggleGroup)
    self._publish_state()
    
    rospy.Timer(rospy.Duration(0.1), self._timer_callback)    
    
  #@timed
  def _emg_callback(self, msg):
    """
    Callback when there is and 'adc' topic. Adds signal to the window buffer.
    """
    self.buffer[0:-1,:] = self.buffer[1:,:]
    self.buffer[-1,:] = msg.signal
    
  def _enumerate(self, req):
    """
    A service call to describe the proportional_emg config.
    """
    channels = []
    for idx, channel in enumerate(self.config["channels"]):
      channels.append(self._create_channel(idx, channel))
      
    return EnumerateResponse(channels=channels)
  
  def _get_state(self, req):
    """
    A service call to return the internal state
    """
    return {"state": State(paused = self.paused)}
  
  def _pause(self, req):
    """
    A service call to pause/unpause the prop_emg controller.
    When paused it will not send signals to the bento_controller.
    Perhaps a better way is to have bento decide if it should listen or ignore a particular node.
    """
    self.paused = req.pause
    self._publish_state()
    return {"paused": self.paused}
  
  def _create_channel(self, idx, channel_def):
    """
    A Channel description used in enumeration
    """
    return Channel(idx = idx,
                   channel=ProportionalEMGController.get_channel_channel( channel_def),
                   threshold=ProportionalEMGController.get_channel_threshold(channel_def),
                   max=ProportionalEMGController.get_channel_max(channel_def),
                   gain=ProportionalEMGController.get_channel_gain( channel_def))
  
  @staticmethod
  def validate_channel(channel_def):
    # TODO: call this from somewhere
    assert "channel" in channel_def
    return True
  
  @staticmethod
  def get_channel_channel(channel_def):
    return channel_def["channel"]
  
  @staticmethod
  def get_channel_threshold(channel_def):
    return channel_def.get("threshold", 1.0)
  
  @staticmethod
  def get_channel_max(channel_def):
    return channel_def.get("max", 5.0)
  
  @staticmethod
  def get_channel_gain(channel_def):
    return channel_def.get("gain", 1.0)
    
  def _scale_channel(self, mavs, channel):
    """
    Scale the signal for a channel.
    Output for each channel is capped at 1.
    """
    sig = mavs[channel["channel"]]
    threshold = channel["threshold"]
    sig = (sig - threshold) if sig > threshold else 0
    
    # calculated the scaled value of sig
    scaled = 0    
    if threshold < channel["max"]:
      scaled = sig/(channel["max"] - threshold)
      
    return min(1, scaled)
  
  def _set_channel_parameters(self, req):
    """
    Sets the config for a particular channel.
    
    Will publish a channel update.
    """
    for channel in req.channels:
      channel_cfg = self.config["channels"][channel.idx]
      channel_cfg["max"] = channel.max
      channel_cfg["threshold"] = channel.threshold
      channel_cfg["gain"] = channel.gain
      self.channel_update_publishers[channel_cfg["channel"]].publish(idx =
                                                                     channel.idx, channel =
                                                                     ProportionalEMGController.get_channel_channel(channel_cfg), max =
                                                                     ProportionalEMGController.get_channel_max(channel_cfg), threshold =
                                                                     ProportionalEMGController.get_channel_threshold(channel_cfg), gain =
                                                                     ProportionalEMGController.get_channel_gain(channel_cfg))
      
    return SetChannelParametersResponse()

  def _timer_callback(self, data):
    """
    The actual function that processes data and sends control messages.
    """
    
    buffer_copy = self.buffer.copy()
    mavs = self._mav(self.buffer)
    for idx, ch in enumerate(self.config["channels"]):
      mavs[idx]*= ProportionalEMGController.get_channel_gain(ch)
      
    scaled = [self._scale_channel(mavs, ch) for ch in self.config["channels"]]
    
    toggling = self._handle_togglers(scaled)

    commands = []
    velocities = []
    
    # TODO: extract all of this into its own class that's agnostic
    # to how it's getting the data and what's done with the data
    for idx, group in enumerate(self.config["joint_groups"]):
      velocity = 0
      
      #if we are currently toggling a joint we do not want send any more commands to it
      if idx not in toggling:
        pos = scaled[group["positive_emg"]]
        neg = scaled[group["negative_emg"]]
        
        if pos or neg and not (pos and neg):
          #velocity = pos if pos > neg else (-neg if neg > pos else 0.0)
          velocity = pos if pos else -neg
      
      velocities.append(velocity)
      commands.append(GroupCommand(group_idx=group["group_idx"],velocity=velocity))

    if not self.paused:
      self.publisher.publish(header=Header(stamp=rospy.Time().now()), commands=commands)
      
    self.mavs_publisher.publish(Float32MultiArray(data=mavs))    
    self.velocity_publisher.publish(Float32MultiArray(data=velocities))
    
  def get_channel_idx(self, channel_num):
    idxs = [idx for idx, channel in enumerate(self.config["channels"] )if channel.channel == channel_num ]
    return None if len(idxs) == 0 else idxs[0]
    
  def _handle_togglers(self, scaled):
    """
    Goes through all configured togglers and toggles if needed.
    """
    toggling = []
    if "toggle" in self.config:      
      for idx, toggle in enumerate(self.config["toggle"]):
        if(scaled[toggle["channel"]] > 0):
          if(not self.toggle_states[idx]):
            self.toggle_states[idx] = True
            if not self.paused:
              self._toggle(toggle["group_idx"], toggle["backward"])
            toggling.append(toggle["group_idx"])
        else:
          self.toggle_states[idx] = False
    return toggling
    
  def get_joint_group(self, group_idx):
    return self.config["joint_groups"][group_idx]
  
  def _toggle(self, group_idx, backward):
    """
    Toggles a single group
    """
    group = self.get_joint_group(group_idx)
    self.toggle_proxy(group_idx = group["group_idx"], backward = backward)
    
  def _mav(self, buffer):
    """
    Calculates the mean absolute value of the buffer
    """
    return np.mean(np.absolute(buffer), 0)
  
  def _publish_state(self):
    """
    publishes the internal state of the proportional_emg_controller
    """
    self.state_publisher.publish(paused = self.paused)

if __name__ == '__main__':
  try:
    rospy.init_node('proportional_emg_controller', anonymous=True)
    ProportionalEMGController()
    rospy.spin()
  except rospy.ROSInterruptException: pass