#!/usr/bin/env python
# license removed for brevity

# Some basic code for interaction between ROS and BURLAP

import rospy
import numpy as np
from std_msgs.msg import String
from burlap_msgs.msg import *

def talker():
	
	# Define the publisher with the BURLAP state message type
    pub = rospy.Publisher('burlap_state', burlap_state, queue_size=10)
    
    # Start the node, with a set rate of 1Hz
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10 Hz
    
    while not rospy.is_shutdown():

    	# Get the current time
        now = rospy.get_rostime()

        # Start a list of values
        agent0values = []

        # Append a value 
        agent0values.append(burlap_value())

        # Give the attribute name and value
        agent0values[0].attribute = "x"
        agent0values[0].value = "1"
        agent0values.append(burlap_value())
        agent0values[1].attribute = "y"
        agent0values[1].value = "2"

        # Start a list of state information details
        stateDetails = []

        # Append a BURLAP object
        stateDetails.append(burlap_object())

        # Define the name, object class and value set
        stateDetails[0].name = "agent0"
        stateDetails[0].object_class = "agent"
        stateDetails[0].values = agent0values

        # Wrap the state information object in BURLAP state message package
        state = burlap_state(objects=stateDetails)

        # Output the state information to the log
        rospy.loginfo(state)

        # Publish the state information to the publisher
        pub.publish(state)

        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass