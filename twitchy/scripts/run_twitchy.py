import rospy

from bento_controller.msg import BentoState
from bento_controller.msg import BentoCommand
from bento_controller.msg import JointCommand
from adc_interface.msg import EMGSignal
from std_msgs.msg import String

import numpy as np

class Twitchy:	

  buffer = np.zeros((10, 4))
  track = 0
  triangle = [float(i)/100 for i in range(-200, 202, 10)]
  triangle.append(triangle.reverse())  

  def __init__(self):
    # rospy.Subscriber("/bento/state", BentoState, self._state_callback)
    rospy.Subscriber("/adc_interface/EMGSignals", EMGSignal, self._emg_callback)
    self.publisher = rospy.Publisher("/bento/command", BentoCommand)
    rospy.Timer(rospy.Duration(0.1), self._timer_callback)


  def _state_callback(self, msg):
    pass

  def _emg_callback(self, msg):
    self.buffer[0:-1,:] = self.buffer[1:,:]
    self.buffer[-1,:] = msg.signal

  def _timer_callback(self, data):
    buffer_copy = self.buffer.copy()
    mavs = self._mav(self.buffer)
    max_speed = 10
    max_voltage = 4
    threshold = 0.3

    command = JointCommand(id=1,type=String('velocity'))
    # Let's say channel 1 moves joint 1 one direction and channel 2 move it the other direction.
    velocity = mavs[0] - mavs[1]
#    velocity = self.triangle[self.track]
 #   self.track += 1
  #  if(self.track >= len(self.triangle) - 1):
   #   self.track = 0
      
    if(abs(velocity)>threshold):
      command.velocity = max_speed*velocity/max_voltage
    print(command.velocity)
    

    self.publisher.publish(BentoCommand([command]))


  def _mav(self, buffer):
    return np.mean(np.absolute(buffer), 0)

if __name__ == '__main__':
  try:
    rospy.init_node('run_twitchy', anonymous=True)
    Twitchy()
    rospy.spin()
  except rospy.ROSInterruptException: pass