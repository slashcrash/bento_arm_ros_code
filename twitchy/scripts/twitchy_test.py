#!/usr/bin/env python

import argparse
import rospy
from std_msgs.msg import *
from bento_controller.srv import *
from bento_controller.msg import *

if __name__=="__main__":
    
    rospy.init_node("bento_test")
    
    pause_srv = rospy.ServiceProxy("/bento/pause", Pause)
    pause_srv.call(False)

    pub = rospy.Publisher('/bento/command', BentoCommand, queue_size=10)

    # pub2 = rospy.Publisher('/burlap_state', )

    rospy.sleep(2)
    
    r = rospy.Rate(1)
    vel = 2.0
    rospy.loginfo("velocity based control")
    for i in xrange(10):
        pub.publish(header=Header(stamp=rospy.Time().now()), 
                    joint_commands=[JointCommand(id=1, type="velocity", velocity=vel), 
                                    JointCommand(id=2, type="velocity", velocity=vel)])
        vel*=-1.0
        
        # sleep is important, let's ROS actually publish
        r.sleep()
    
    rospy.sleep(2)
    rospy.loginfo("Velocity control complete... waiting for command.")
    # rospy.loginfo("switching to position based control")
    
    # pos = [0.0, 4.0]
    # for i in xrange(10):
    #     idx = i % 2
        
        
    #     # sending position and velocity command. If you just send a position command it uses the last set velocity, which might be zero
    #     pub.publish(header=Header(stamp=rospy.Time().now()), 
    #                 joint_commands=[JointCommand(id=1, type="position_and_velocity", position=pos[idx], velocity=2.0), 
    #                                 JointCommand(id=2, type="position_and_velocity", position=pos[idx], velocity=2.0)])
    #     r.sleep()