package myoelectricarm.utils;

import rlpark.plugin.robot.internal.disco.drops.Drop;
import rlpark.plugin.robot.internal.disco.drops.DropData;
import rlpark.plugin.robot.internal.disco.drops.DropFloat;

public abstract class CommandSensorDrops {

	public static Drop newSensorDrop(String namePrefix, String [] definition) {
		return createDrop(namePrefix + "SensorDrop", definition);
	}
	
	public static Drop newCommandDrop(String namePrefix, String [] definition) {
		return createDrop(namePrefix + "CommandDrop", definition);
	}

	// TODO: I think this will probably need to be more robust to handle different types, but so far all I've seen for the MTTDrops is floats
	protected static Drop createDrop(String dropName, String [] definition) {
		DropData [] descriptors = new DropData [definition.length];
		for(int i = 0; i < definition.length; i++) {
			descriptors[i] = new DropFloat(definition[i]);
		}
		return new Drop(dropName, descriptors);
	}
}


