package myoelectricarm.utils;

import rlpark.plugin.robot.internal.disco.drops.Drop;


public class TwitchyDrops {
	
	protected static String [] sensorDefinition = {"RotationPosition", "ElevatorPosition"};
	// TODO: I am tempted to turn this into 2 different commands - one for position based control and the other
	// for velocity
	protected static String [] commandDefinition = {"VelRotation", "PosRotation", "VelElevator", "PosElevator"};
	protected static String namePrefix = "Twitchy";
	
	public static Drop commandDrops;
	public static Drop sensorDrops;
	
	static {
		commandDrops = CommandSensorDrops.newCommandDrop(namePrefix, commandDefinition);
		sensorDrops = CommandSensorDrops.newSensorDrop(namePrefix, sensorDefinition);
	}
	
	
}
