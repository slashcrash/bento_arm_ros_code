package myoelectricarm.utils;

import rlpark.plugin.rltoys.envio.actions.ActionArray;

public class TwitchyTest {

	public static void main(String[] args) {
		Twitchy twitchy = new Twitchy("patisserie", 7000, true);
		
		double [] obs;
		
		ActionArray actions [] = {new ActionArray(1, 0, 1, 0),
				new ActionArray(1, 2, 1, 2),
				new ActionArray(1,0,1,4),
				new ActionArray(1,4,1,0)};
		
		int count = 0;
		int action = 0;
		while(true) {
			if(count % 10 == 0) {
				if(action == 4) {
					action = 0;
				}
				twitchy.sendAction(actions[action]);
				action++;
			}
			obs = twitchy.waitNewObs();
			System.out.println(String.format("%f %f", obs[0], obs[1]));
			count++;
		}
	}

}
