package myoelectricarm.utils;

import java.io.IOException;
import java.nio.ByteOrder;

import rlpark.plugin.rltoys.envio.actions.Action;
import rlpark.plugin.rltoys.envio.actions.ActionArray;
import rlpark.plugin.rltoys.envio.observations.Legend;
import rlpark.plugin.robot.helpers.RobotEnvironment;
import rlpark.plugin.robot.internal.disco.DiscoConnection;
import rlpark.plugin.robot.internal.disco.datagroup.DropScalarGroup;
import rlpark.plugin.robot.internal.disco.drops.Drop;
import zephyr.plugin.core.api.monitoring.annotations.Monitor;

@Monitor
public class Bento extends RobotEnvironment {
	private static Drop sensorDrop = BentoDrops.newSensorDrop();
	private static Drop actionDrop = BentoDrops.newCommandDrop();
	private static DropScalarGroup actions = new DropScalarGroup(actionDrop);
	private final DiscoConnection connection;

	public Bento(String hostname, int port, boolean persistent) {
		super(new DiscoConnection(hostname, port, sensorDrop, ByteOrder.LITTLE_ENDIAN), persistent);
		connection = (DiscoConnection) super.receiver();
	}

//	@Override
	public void sendAction(Action a) {
		actions.set(((ActionArray) a).actions);
		try {
			connection.socket().send(actionDrop);
		} catch (IOException e) {
			connection.close();
			e.printStackTrace();
		}
	}

	@Override
	public Legend legend() {
		return connection.legend();
	}

}
