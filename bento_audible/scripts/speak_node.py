#!/usr/bin/env python

import rospy
from bento_controller.msg import *
from bento_controller.srv import *

from bento_audible import speaker

class SpeakNode(object):
  ros_prefix = "bento_audible"
  def __init__(self):
    self.speaker = speaker.Speaker(rospy.get_param(self.ros_prefix))
    
    enumerate_prx = rospy.ServiceProxy("/bento/enumerate", Enumerate)
    enumeration = enumerate_prx.call()

    self.joints = {}
    for joint in enumeration.joints:
      self.joints[joint.id] = {"nice_name": SpeakNode._get_nice_name(joint),
                                  "name": SpeakNode._get_name(joint)}
    
    rospy.Subscriber("/bento/selected_joint", JointSelection, self._selection_callback, queue_size=1)
    
  @staticmethod
  def _get_name(joint):
    return joint.name
  
  @staticmethod
  def _get_nice_name(joint):
    return joint.nice_name
    
  def _selection_callback(self, msg):
    joint = self.joints[msg.joint_id]
    self.speaker.speak(joint)

if __name__ == '__main__':
  try:
    rospy.init_node('speak', anonymous=True)
    rospy.wait_for_service("/bento/enumerate")
    SpeakNode()
    rospy.spin()
  except rospy.ROSInterruptException: pass
    