#!/usr/bin/env python

import pygame

class Speaker(object):

    def __init__(self, config):
        self.config = config
        pygame.mixer.init()
    
    def speak(self, joint):
        file_name = self.config[joint["name"]]
        pygame.mixer.music.load(file_name)
        pygame.mixer.music.play()
        