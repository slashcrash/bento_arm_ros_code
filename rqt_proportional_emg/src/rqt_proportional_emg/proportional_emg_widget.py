#!/usr/bin/env python

import rospy
import rospkg
import os
from python_qt_binding import loadUi
from python_qt_binding.QtCore import Qt
from python_qt_binding.QtGui import QWidget, QIcon, QLabel

from proportional_emg.srv import *
from .channel_widget import ChannelWidget

import bento_controller.srv

class ProportionalEMGWidget(QWidget):
    
    def __init__(self, context):
        super(ProportionalEMGWidget, self).__init__()
        self.context = context
        
        rp = rospkg.RosPack()
        ui_file = os.path.join(rp.get_path('rqt_proportional_emg'), 'resource', 'proportional_emg.ui')
        loadUi(ui_file, self)
        self.setObjectName('ProportionalEMGWidget')
        
        self.play_icon = QIcon.fromTheme('media-playback-start')
        self.pause_icon = QIcon.fromTheme('media-playback-pause')
        self.pause_button.clicked[bool].connect(self._handle_pause_clicked)        
        
        self.enumerate_srv = rospy.ServiceProxy("/proportional_emg/enumerate", Enumerate)        
        self.set_channel_parameters_srv = rospy.ServiceProxy("/proportional_emg/set_channel_parameters", SetChannelParameters)
        self.get_state_srv = rospy.ServiceProxy("/proportional_emg/get_state", GetState)
        self.pause_srv = rospy.ServiceProxy("/proportional_emg/pause", Pause)
        state = self.get_state_srv.call()
        self.paused = state.state.paused
        self.enumeration = self.enumerate_srv()
        
        rospy.Subscriber("/proportional_emg/state", proportional_emg.msg.State, self._state_callback)
        
        self._load_channels()
        self._update_pause_icon()
        self.update()
        
    def _handle_pause_clicked(self):
        resp = self.pause_srv(pause = (not self.paused))
        self.paused = resp.paused
        self._update_pause_icon()
        
    def _load_channels(self):
        print(self.enumeration.channels)
        for channel in self.enumeration.channels:
            channel_widget = ChannelWidget(channel, self._update_channel_callback)
            self.channel_tab.addTab(channel_widget, "%d" % (channel.channel + 1))
            
    def _state_callback(self, msg):
        self.paused = msg.paused
        self._update()
            
    def _update(self):
        self._update_pause_icon()
        
    def _update_pause_icon(self):
        if(self.paused):
            self.pause_button.setIcon(self.play_icon)
        else:
            self.pause_button.setIcon(self.pause_icon)
            
    def _update_channel_callback(self, data):
        self.set_channel_parameters_srv(channels=[data])            
