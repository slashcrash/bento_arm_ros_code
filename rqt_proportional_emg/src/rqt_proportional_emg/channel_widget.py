#!/usr/bin/env python

import rospy
import rospkg
import os
from python_qt_binding import loadUi
from python_qt_binding.QtCore import Qt
from python_qt_binding.QtGui import QWidget, QIcon, QLabel, QSlider
from rqt_plot.plot_widget import PlotWidget
from rqt_plot.plot import Plot

from proportional_emg.msg import Channel

class ChannelWidget(QWidget):
    
    hard_max = 5
 
    def __init__(self, channel, value_change_callback):
        super(ChannelWidget, self).__init__()
        
        self.channel = channel
        self.value_change_callback = value_change_callback
        
        rp = rospkg.RosPack()
        ui_file = os.path.join(rp.get_path('rqt_proportional_emg'), 'resource', 'channel.ui')
        loadUi(ui_file, self)
        self.setObjectName('ChannelWidget')
        
        self.threshold_input.setKeyboardTracking(False)
        self.max_input.setKeyboardTracking(False)
        self.gain_input.setKeyboardTracking(False)
    
        self.threshold_input.valueChanged.connect(self._handle_threshold_change)
        
        self.max_input.valueChanged.connect(self._handle_max_change)
        
        self.gain_input.valueChanged.connect(self._handle_gain_change)
        
        self._update_display()
        
        self.outer_layout.argv = self._get_argv
        self.outer_layout.serial_number = lambda : 0
        self.outer_layout.add_widget = self.outer_layout.addWidget
        self.plot_plugin = Plot(self.outer_layout)        
        
        rospy.Subscriber("/proportional_emg/channels/%d" % self.channel.channel, Channel, self._channel_update_handler)
        
    def _channel_update_handler(self, msg):
        self.channel = msg
        self._update_display()
        
    def _get_argv(self):
        return ("", "/proportional_emg/mavs/data[%s]" % self.channel.channel)
    
    def _update_display(self):
        self._update_threshold_value_label()
        self._update_max_value_label()
        self._update_gain_value_label()
        
    def _handle_gain_change(self, data):
        if(data != self.channel.gain):
            self.channel.gain = data
            self.value_change_callback(self.channel)
        
    def _update_gain_value_label(self):
        self.gain_input.setValue(self.channel.gain)
    
    def _update_threshold_value_label(self):
        print(self.channel.threshold)
        self.threshold_input.setValue(self.channel.threshold)
        
    def _handle_threshold_change(self, data):
        if (data != self.channel.threshold):
            self.channel.threshold = data
            self.value_change_callback(self.channel)
        
    def _update_max_value_label(self):
        self.max_input.setValue(self.channel.max)
        
    def _handle_max_change(self, data):
        if (data != self.channel.max):
            self.channel.max = data
            self.value_change_callback(self.channel)