#!/usr/bin/env python
__author__ = 'Craig Sherstan'

"""
Test to figure out how to control.
"""

import rospy
from std_msgs.msg import Header
from sensor_msgs.msg import JointState

if __name__ == "__main__":
    pub = rospy.Publisher('/bento/joint_states', JointState, queue_size=1)
    rospy.init_node('go_home', anonymous=True)
    
    header = Header()
    header.stamp = rospy.Time.now()
    pub.publish(header = header, position = [0], name = ['Shoulder'],
                velocity = [], effort=[])
    