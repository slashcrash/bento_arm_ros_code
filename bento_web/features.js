this.ros_web = this.ros_web || {};

_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

ros_web.id_counter = 0;

ros_web.create_id = function (parent_id, append_name) {
    name = parent_id + '_' + append_name + '_' + ros_web.id_counter;
    ros_web.id_counter +=1;
    return name;
};

(function(){
    var FeatureGridPlugin = function(parent, ros) {
        this.me = this;
        this.ros = ros;
        this.topics = {};
        this.id_counter = 0;
    
        this.parent = $(parent);
        this.parent_id = this.parent.attr("id");
    
        this.template = "<div id='{{plugin_id}}'>\
            <form class='form-horizontal' onsubmit='return false;'>\
                <div class='form-group'>\
                    <div class='col-sm-10'>\
                        <input type=text class='form-control' id='{{add_topic_input}}'></input>\
                    </div>\
                    <button type='button' class='col-sm-2 btn btn-primary' id='{{add_topic_button}}'>Add</button>\
                </div>\
            </form>\
            <div id='{{output_container}}'></div>\
        </div>";
    
        this.render();
    };

    var p = FeatureGridPlugin.prototype;

    p.render = function() {
        var temp = _.template(this.template);

        this.plugin_id = ros_web.create_id(this.parent_id, "feature_grid_plugin");
        var add_topic_input_id = ros_web.create_id(this.parent_id, "add_topic_input");
        var add_topic_button_id = ros_web.create_id(this.parent_id, "add_topic_button");
        var output_container_id = ros_web.create_id(this.parent_id, "output_container");
    
        this.container = temp({"plugin_id": this.plugin_id, "add_topic_input": add_topic_input_id,
            "add_topic_button": add_topic_button_id, "output_container": output_container_id});
    
        this.parent.append(this.container);
    
        this.container = $("#" + this.plugin_id);
    
        this.add_topic_input = $("#" + add_topic_input_id, this.container);
        this.add_topic_button = $("#" + add_topic_button_id, this.container);
        this.output_container = $("#" + output_container_id, this.container);
    
        var me = this;
        this.add_topic_button.click(function(){me.add_topic_button_click()});
        this.add_topic_input.keyup(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
                e.preventDefault();							 
                me.add_topic_button_click();
                return false;
            }});
    };                                  

    p.add_topic_button_click = function() {
        var topic = this.add_topic_input.val();
        if (topic) {
            this.add_topic(topic);
        }
    };

    p.add_topic = function(topic_name) {
        if(!(topic_name in this.topics)) {
            var me = this;
            this.topics[topic_name] = new ros_web.FeatureGrid(this.output_container, topic_name, this.ros, 
                function(topic){
                    me.topic_removed_callback(topic);
                });
        }
    };

    p.topic_removed_callback = function(topic) {
        delete this.topics[topic]
    };

    ros_web.FeatureGridPlugin = FeatureGridPlugin;

}());

(function(){
    function FeatureGrid(parent, topic_name, ros, post_remove_callback) {
        this.parent = parent;
        this.parent_id = this.parent.attr("id");
        this.topic_name = topic_name;
        this.ros = ros;
        this.post_remove_callback = post_remove_callback;
        this.feature_size = 0;
        this.min_width = 3;
        this.bin_height = 10;
        this.last_message = {"indices": []};
        this.delta_buffer = [];
        this.buffer_max_length = 2;
        this.buffer_length = 0;
        
        this.non_zero_count = 0;
        this.non_zero_mean = 0;
        
        this.max_delta = 0;
        this.min_delta = null;

        this.template = "<div id={{grid_id}} class='container'>\
                            <canvas id={{canvas_id}} width='{{width}}' height='{{height}}'></canvas>\
                            <div class='row'>\
                                <div class='col-md-10'>{{topic}}</div>\
                                <div class='col-md-1 col-md-offset-1'>\
                                    <button id='{{remove_button_id}}' type='button' class='btn btn-danger btn-xs'>\
                                        <span class='glyphicon glyphicon-remove'></span>\
                                    </button>\
                                </div>\
                            </div>\
                            <div class='row'>\
                                <div class='col-md-1 col-md-offset-1'>Length:</div>\
                                <div class='col-md-1' id={{length_label_id}}></div>\
                            </div>\
                            <div class='row'>\
                                <div class='col-md-1 col-md-offset-1'>Active:</div>\
                                <div class='col-md-1' id={{active_label_id}}></div>\
                            </div>\
                            <div class='row'>\
                                <div class='col-md-1 col-md-offset-1'>Delta:</div>\
                                <div class='col-md-1' id={{delta_label_id}}></div>\
                            </div>\
                        </div>"
    
        this.render();
    
        this.listener = new ROSLIB.Topic({
            ros : ros,
            name : this.topic_name,
            messageType : 'ros_utils/SparseBinaryFeatures',
            queue_size : 1,
            throttle_rate : 1
        });
    
        var me = this;
        this.listener.subscribe(function(message){me.callback(message)});
    };

    p = FeatureGrid.prototype;

    p.render = function() {
        var temp = _.template(this.template);
        var width = this.parent.innerWidth()
        this.container_id = ros_web.create_id(this.parent_id, "feature_grid");
        this.canvas_id = ros_web.create_id(this.container_id, "canvas");
        this.remove_button_id = ros_web.create_id(this.container_id, "remove_button");
        this.length_label_id = ros_web.create_id(this.container_id, "length_label");
        this.active_label_id = ros_web.create_id(this.container_id, "active_label");
        this.delta_label_id = ros_web.create_id(this.delta_label_id, "delta_label");
        this.parent.append(temp({"grid_id": this.container_id, "canvas_id": this.canvas_id, 
            "width": width, "height": this.bin_height, 
            "topic": this.topic_name,
            "remove_button_id": this.remove_button_id, "length_label_id": this.length_label_id, 
            "active_label_id": this.active_label_id, "delta_label_id": this.delta_label_id}));
        this.container = $("#" + this.container_id);
        this.canvas = $("#" + this.canvas_id);

        var me = this;
        $("#" + this.remove_button_id).click(function(){me.remove();});
    
        this.stage = new createjs.Stage(this.canvas[0]);
    
        this.background = new createjs.Shape();
        this.background.graphics.beginFill("black").drawRect(0, 0, this.get_width(), this.get_height());
        this.stage.addChild(this.background);
        this.stage.update();
    };

    p.callback = function(message) {
        var first_time = false;
        if(!this.feature_size) {
            this.feature_size = message.size;
            $("#" + this.length_label_id).html(this.feature_size);
            this.initialize_grid();
            first_time = true;
        }
        
        $("#" + this.active_label_id).html(message.indices.length)

        turn_on = _.difference(message.indices, this.last_message.indices);
        turn_off = _.difference(this.last_message.indices, message.indices);
        
        
        // DELTA calculations
        var delta = turn_on.length + turn_off.length;
        if(!first_time){
            if (this.delta_buffer.length == this.buffer_max_length) {
                this.delta_buffer.shift();
            }
            this.delta_buffer.push(delta);
            this.buffer_length++;
        }
        
        var delta_total = 0;
        $.each(this.delta_buffer, function(){
            // this is the value here
            delta_total += this;
        });


        
        if(!first_time) {
            if(delta) {
                this.non_zero_count++;            
                this.non_zero_mean = this.non_zero_mean + (delta - this.non_zero_mean)/this.non_zero_count;
            }
            if(delta > this.max_delta) {
                this.max_delta = delta;
            }
            
            if(delta && (this.min_delta==null || this.min_delta > delta)) {
                this.min_delta = delta;
            }
        }
        
        $("#" + this.delta_label_id).html(Math.round(delta_total/this.buffer_length) + '/ts ' + Math.round(this.non_zero_mean) + '/nzdelta Max:' + this.max_delta + ' Min: ' + this.min_delta);
    
        for (i = 0; i < turn_on.length; i++) {
            var cell = this.cells[turn_on[i]];
            cell.visible = true;
        }
    
        for (i = 0; i < turn_off.length; i++) {
            var cell = this.cells[turn_off[i]];
            cell.visible = false;
        }
    
        this.stage.update();
        this.last_message = message;
    };

    p.initialize_grid = function() {
        this.cells = [];
        var cells_per_row = this.calc_num_per_row();
        var x = 0, y = 0;
        for(i = 0; i < this.feature_size; i++) {
            var rect = new createjs.Shape();
            rect.graphics.beginFill("green").drawRect(x, y, this.min_width, this.bin_height);
            rect.visible = false;
            this.stage.addChild(rect);
            this.cells.push(rect);

            x += this.min_width;
        
            if (i % cells_per_row == (cells_per_row - 1)) {
                y += this.bin_height;
                x = 0;
            }
        }

        var height = this.calc_num_rows() * this.bin_height;
    
        this.canvas.attr("height", height);
        this.background.graphics.beginFill("black").drawRect(0,0,this.get_width(), height);
    };

    p.calc_row = function(index) {
        return math.floor(index/this.calc_num_per_row());
    };

    p.calc_num_rows = function() {
        return this.feature_size/this.calc_num_per_row() + 1;
    };

    p.calc_col = function(index) {
        return index % this.calc_num_per_row();
    };

    p.calc_num_per_row = function() {
        return this.get_width() / this.min_width;
    };

    p.get_width = function() {
        return this.canvas.attr("width");
    };

    p.get_height = function() {
        return this.canvas.attr("height");
    };

    p.remove = function() {
        this.listener.unsubscribe();
        this.container.remove();
        this.stage.clear();
        this.stage = null;

        if(this.post_remove_callback) {
            this.post_remove_callback(this.topic_name);
        }
    };

    ros_web.FeatureGrid = FeatureGrid;
}());