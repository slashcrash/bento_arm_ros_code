import rospy
import rospkg
import os
from python_qt_binding import loadUi
import python_qt_binding.QtCore as QtCore
from python_qt_binding.QtGui import QWidget, QIcon, QTableWidgetItem, QPushButton
from bento_controller.srv import *

# TODO: Not a real Widget, the name is misleading
class JointWidget(QtCore.QObject):
    update_signal = QtCore.Signal()
    
    def __init__(self, idx, joint, table):
        super(JointWidget, self).__init__()        
        
        # row number
        self.idx = idx
        self.joint = joint
        self.table = table
        self._init_table()
        self.torque_srv = rospy.ServiceProxy("/bento/torque_enable", TorqueEnable)
        self.go_home_srv = rospy.ServiceProxy("/bento/go_home", GoHome)
        self.torque_enabled = True
        
        def __update():
            self.update_torque_button()
            
        self.update_signal.connect(__update)
        
    def _init_table(self):
        self.table.setItem(self.idx, 0, QTableWidgetItem(self.joint.name))
        self.temp_item = QTableWidgetItem()
        self.table.setItem(self.idx, 1, self.temp_item)
        self.load_item = QTableWidgetItem()
        self.table.setItem(self.idx, 2, self.load_item)
        self.position_item = QTableWidgetItem()
        self.table.setItem(self.idx, 3, self.position_item)
        self.torque_button = QPushButton("T")
        self.torque_button.clicked[bool].connect(self._handle_torque_click)
        self.table.setCellWidget(self.idx, 4, self.torque_button)
        self.home_button = QPushButton()
        self.home_button.setIcon(QIcon.fromTheme('go-home'))
        self.home_button.clicked[bool].connect(self._handle_home_click)
        self.table.setCellWidget(self.idx, 5, self.home_button)
        
    def update_state(self, state):        
        self.temp_item.setText("%s" % state.motor_temps)
        self.load_item.setText("%.3f" % state.load)
        self.position_item.setText("%.3f" % state.current_pos)
        self.torque_enabled = state.torque_enabled 
        self.update_signal.emit()
        
    def update_torque_button(self):
        if self.torque_enabled:
            self.torque_button.setStyleSheet("background-color: green")
        else:
            self.torque_button.setStyleSheet("background-color: grey")
            
    def _handle_home_click(self):
        self.go_home_srv.call(apply_to=[self.joint.id])
        
    def _handle_torque_click(self):
        self.torque_srv.call(enable=not self.torque_enabled, apply_to=[self.joint.id])
        self.torque_enabled = not self.torque_enabled
        