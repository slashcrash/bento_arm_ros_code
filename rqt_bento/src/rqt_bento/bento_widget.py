import rospy
import rospkg
import os
from python_qt_binding import loadUi
from python_qt_binding.QtCore import Qt
from python_qt_binding.QtGui import *
from bento_controller.msg import *
from bento_controller.srv import *
from .joint_widget import JointWidget
import traceback

class BentoWidget(QWidget):
    def __init__(self, context):
        super(BentoWidget, self).__init__()
        
        rp = rospkg.RosPack()
        # I really don't like this style of gui design where we use resource files
        # It makes sense if you're using a designer, but I'm not. In hindsight I
        # would ditch the .ui files and just do everything programmatically
        ui_file = os.path.join(rp.get_path('rqt_bento'), 'resource', 'bento.ui')
        loadUi(ui_file, self)
        self.setObjectName('BentoWidget')        
        
        self.play_icon = QIcon.fromTheme('media-playback-start')
        self.pause_icon = QIcon.fromTheme('media-playback-pause')
        self.home_button.setIcon(QIcon.fromTheme('go-home'))
        
        self.pause_button.clicked[bool].connect(self._handle_pause_clicked)
        self.torque_button.clicked[bool].connect(self._handle_torque_clicked)
        self.home_button.clicked[bool].connect(self._handle_home_clicked)
        
        # services
        self.pause_srv = rospy.ServiceProxy("/bento/pause", Pause)
        self.is_paused_srv = rospy.ServiceProxy("/bento/is_paused", IsPaused)
        self.torque_srv = rospy.ServiceProxy("/bento/torque_enable", TorqueEnable)
        self.go_home_srv = rospy.ServiceProxy("/bento/go_home", GoHome)
        self.set_meta_srv = rospy.ServiceProxy("/bento/set_meta", SetMeta)
        
        self.is_paused = self.is_paused_srv.call().state
        self._set_pause_icon()
        
        self.load_joints()
        self.torque_enabled = True
        
        self.create_meta_area()

        self.subscribers = []
        self.subscribers.append(rospy.Subscriber("/bento/state", BentoState, self._state_callback))
        self.subscribers.append(rospy.Subscriber("/bento/meta", Meta, self._meta_callback))

        self.last_meta = ''
    def shutdown(self):
        for sub in self.subscribers:
            sub.unregister()
        
    def load_joints(self):
        proxy = rospy.ServiceProxy("/bento/enumerate", Enumerate)
        self.enumerated_joints = proxy()
        self.joint_widgets = {}
        self.joint_states_table.horizontalHeader().setVisible(True)
        self.joint_states_table.verticalHeader().setVisible(False)
        self.joint_states_table.setRowCount(len(self.enumerated_joints.joints))
        self.joint_states_table.setColumnCount(6)
        
        for idx, joint in enumerate(self.enumerated_joints.joints):
            print joint
            self.joint_widgets[joint.id] = JointWidget(idx, joint, self.joint_states_table)
            
        self.joint_states_table.setHorizontalHeaderLabels(("Joint", "Temp", "Load", "Position", "Torque", "Home"))           
            
        tableWidth = self.joint_states_table.verticalHeader().width() + 2
        for i in range(self.joint_states_table.columnCount()):
            tableWidth += self.joint_states_table.columnWidth(i)
            
   
        tableHeight = self.joint_states_table.horizontalHeader().height() + 2
        for i in range(self.joint_states_table.rowCount()):
            tableHeight += self.joint_states_table.rowHeight(i)            
   
        self.joint_states_table.setMaximumHeight(tableHeight)
        self.joint_states_table.setMinimumHeight(tableHeight)
        self.joint_states_table.setMaximumWidth(tableWidth)
        self.joint_states_table.setMinimumWidth(tableWidth)
            
    def create_meta_area(self):
        line_layout = QHBoxLayout()
        line_layout.addWidget(QLabel("Meta"))
        self.meta_line_edit = QLineEdit()
        line_layout.addWidget(self.meta_line_edit)
        
        self.meta_layout.addLayout(line_layout)

        button_layout = QHBoxLayout()
        button_layout.addStretch()

        set_button = QPushButton("Set")
        set_button.clicked[bool].connect(self.set_meta_clicked)
        button_layout.addWidget(set_button)

        sync_button = QPushButton("Sync")
        sync_button.clicked[bool].connect(self.sync_meta_clicked)
        button_layout.addWidget(sync_button)

        self.meta_layout.addLayout(button_layout)
        
    def set_meta_clicked(self):
        self.set_meta_srv.call(meta = self.meta_line_edit.text())

    def sync_meta_clicked(self):
        self.meta_line_edit.setText(self.last_meta)
        
    def _meta_callback(self, msg):
        if self.meta_line_edit.text != msg.meta:
            self.meta_line_edit.setText(msg.meta)

    def _handle_home_clicked(self):
        self.torque_enabled = True
        self.go_home_srv()
        
    def _handle_pause_clicked(self):
        resp = self.pause_srv(state = (not self.is_paused))
        self.is_paused = resp.state
        self._set_pause_icon()
        
    def _handle_torque_clicked(self):
        self.torque_enabled = not self.torque_enabled
        self.torque_srv.call(enable=self.torque_enabled)
        
    def _set_pause_icon(self):
        if(self.is_paused):
            self.pause_button.setIcon(self.play_icon)
        else:
            self.pause_button.setIcon(self.pause_icon)
            
    def _state_callback(self, msg):
        self.is_paused = msg.paused
        self._set_pause_icon()
        if self.last_meta != msg.meta:
            self.last_meta = msg.meta
            self.meta_line_edit.setText(msg.meta)
        for state in msg.joint_states:
            widget = self.joint_widgets[state.joint_id]
            widget.update_state(state)
            