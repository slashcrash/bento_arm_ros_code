# Building #

Note that presently this software relies on a custom fork for the dynamixel code: https://github.com/csherstan/dynamixel_motor. Clone that repo into your workspace.


### The Arm
- Open up a terminal and run 

    roscore

- In another terminal run 
    
    roslaunch bento bento.launch
    
### GUI

At present there are 3 GUIs for interacting with Bento, which can either be run directly from command line or 
from the plugins menu in rqt.

1. rqt_bento - Provides three buttons: pause/play, torque, and home. Allows you to set the "meta" data associated associated with the bento state messages, which is useful when recording data for postprocessing. Also provides some feedback from the joints.

2. rqt_proportional_emg - use this to interact with the proportional emg controller.

3. rqt_bento_groups - Shows the configured joint groups and allows you to select which joint is active in each group. Displays the currently active joint in each group.

    
## Configuration

Configuration is done through a combination of launch files and yaml files. While there are some defaults in the code base already, if
you are making some configurations that are just for your own purposes, rather than for everyone to use, you should probably create 
a new ros package to place them in.

The default files are described below:

bento_controller/cfg/joints.yaml - defines the joints used by the bento arm.

TODO: finish this

### bento

No actual code here, just lots of launch and config files. Includes some sample audio files to use for bento_audible.

### bento_audible

Listens for group selection changes and audibly tells the user which joint is selected. Depends on PyGame. 

Params:

Specify the file to use with each joint. Ex from bento\launch\audible.launch:


```
#!xml

    <arg name="audio_directory" default="$(find bento)/audio"/>
    <rosparam param="bento_audible" subst_value="True">
        shoulder_rotation: "$(arg audio_directory)/shoulder_rotation.mp3"
        elbow_flexion: "$(arg audio_directory)/elbow_flexion.mp3"
        wrist_rotation: "$(arg audio_directory)/wrist_rotation.mp3"
        wrist_flexion: "$(arg audio_directory)/wrist_flexion.mp3"
        gripper: "$(arg audio_directory)/gripper.mp3"
    </rosparam>
```

### bento_controller

The actual code for controlling the arm.

Configuration:

Here the configuration is a bit more complicated than it should be. It uses command line args as opposed to ros params, this should be changed.

Joints must be defined as ROS Params in the format:


```
#!yaml

shoulder_rotation_controller:
    type: dynamixel
    nice_name: shoulder
    joint_name: shoulder_rotation
    joint_speed: 2.0
    joint:
        id: 1
        init: 0
        min: 1028
        max: 3073
```

At the moment, only Dynamixel joints are defined, but this will change. I expect this overall format to change. In particular, I expect to define a joint id that is independent of the type.

Now when launching the node it is necessary to specify parameters as command line args (this will be changed).

--manager dxl_manager : specify the name of the dynamixel manager used to launch the dynamixel controller manager

--port bento: name of the port used for the controller manager

--wait: adding this flag will cause the controller to launch in the paused state, meaning it won't accept any direct movement commands (other than go_home).

controller_name_1,controller_name_2: comma separated list of controller names

#### groups

Joints can be controlled individually or in groups. Currently the joint grouping control is integrated into bento_controller. However, I think that it will have to be refactored out into its own package. Anyways, the groups are configured as rosparams like:


```
#!yaml
bento_controller:
    joint_groups:
        - {name: "all", joints:[
            {id: 1, max_speed: 1, min_speed: 0},
            {id: 2, max_speed: 1, min_speed: 0},
            {id: 3, max_speed: 2, min_speed: 0},
            {id: 4, max_speed: 2, min_speed: 0},
            {id: 5, max_speed: 2, min_speed: 0}]}
```

bento_controller defines joint_groups, which contains a list of groups. Each group has a name and a list of joints. A joint is defined by id (joint_id), max_speed: the max speed the joint can go, and min_speed: the minimum turn on speed.

When commanding groups the group_idx is the zero based index of the group as it appears in this joint_groups list.

#### publishes

/bento/state - BentoState.msg, the state of the Bento arm, includes joint state information, whether the arm is paused or not and whether or not the torque is enabled. Additionally, includes the last set meta information.

/bento/selected_joint - JointSelection.msg, The joint selected in a joint_group.

/bento/meta - Meta.msg, This is published when the meta information is changed. Meta is a way of adding some additional information to the data stream. I use it for markers during experiments. For example, if a am lifting two different weights with the arm I might set the weight in the meta before each trial. Meta can be set from rqt_bento GUI.

/bento/joint_group_update - JointGroup.msg. Publishes the definition of a JointGroup when the configuration of the group is changed.

#### command

/bento/command - BentoCommand.msg, Used to control specific joints directly (not grouped). Can pass in a single joint or multiple joints. Joint can be controlled by velocity or position. Contains an array of JointCommand. JointCommands take a "type" parameter, at present this can be "velocity", "position", and "position_and_velocity". The id field is the joint id. Specify velocity and position according to the command type. velocity is given in rad/s (I think), and position is given in rads (I think).

/bento/group/command - BentoGroupCommand.msg. Used to send velocity commands to a joint_group. Contains and array of GroupCommand.msg. The velocity here should be [-1,1] as it will be scaled by the max and min speeds specified in the config for the joint in the joint_group

#### services

/bento/pause - Pause and Resume the arm. Will not listen to any BentoCommand or BentoGroupCommands when paused.

/bento/is_paused - returns whether or not the arm is paused

/bento/enumerate - call to get a configuration description. I think in future we may want to move away from this in favor of just using rosparams

/bento/torque_enable - Call to enable and disable torque. When torque is toggled the arm is also paused.

/bento/go_home - Tells the arm to go home. Arm is paused and must be unpaused before using again.

/bento/toggle_group - Toggles a joint_group, either forwards or backwards through the list

/bento/select_joint - selects a specific joint in a joint_group

/bento/set_meta - sets the meta data string that is published with /bento/state

/bento/configure_group - specify the configuration for a specific joint group

### bento_dynamixel

A multi mode controller for dynamixel joints.

### joy_to_bento


```
#!bash

sudo install ros-hydro-joystick-drivers

ls /dev/input
```

see inputs starting with jsX. You can guess the X, based on which light is lit up on the xbox controller. If the first player light is lit up then it's js0, add 1 for each different player light.


```
#!bash

sudo chmod a+rw /dev/input/jsX
```

Configuration:

Specified using rosparams. See the example .yaml files in the package for explanations. Controls can be mapped be configuration to control the various predefined actions on Bento. Individual joints can be controlled individually, or groups can be controlled.

```
#!yaml

joy_to_bento:
    controller: "xbox_wired"
    mappings:
        - {action: "pause", control: "a"}
        - {action: "go_home", control: "b"}
        - {action: "torque", control: "x"}
        - {action: "toggle", control: "right-trigger", offset: true, config: {backward: false, group_idx: 0}}
        - {action: "group_command", control: "left-y", threshold: 0.2, config: {group_idx: 0}}
       #- {action: "command", control: "left-y", config: {joint_id: 0, invert: true}, threshold: 0.2}
```

For using a sixaxis PS3 controller see: https://help.ubuntu.com/community/Sixaxis

Note that you cannot use regular bluetooth while using the sixad drivers.

### proportional_emg

An interface for controlling the arm via a proportional EMG controller

#### publishes

/proportional_emg/velocities - The velocities that it calculates for each channel. Used for debugging.

/proportional_emg/mavs - The Mean Absolute Values of each channel. Used for debugging.

/proportional_emg/state - The state of the proportional_emg controller. Just says whether or not it is paused

#### services

/proportional_emg/enumerate - Spits out configuration information.

/proportional_emg/set_channel_parameters - Call to set the channel parameters: gain, threshold, max

/proportional_emg/pause - Call to Pause and Resume. When paused it will not publish to /bento/group/command or toggle groups

/proportional_emg/get_state - Call to get the state of controller.

### rlpark

Deprecated

An experiment used in conjunction with twitchy_bridge where rostopics were written into a format expected by rlpark
and transmitted via TCP/IP.

### rqt_bento

GUI for Bento. Pause, Torque, Go Home, Joint States, set meta.

Meta is used to add information to the state reported by Bento.

### rqt_bento_groups

GUI for controlling the bento groups

### rqt_proportional_emg

GUI for proportional_emg

### twitchy

Config and launch files for running twitchy

### twitchy_bridge

Deprecated - do not use

Published ROS topics across TCP/IP, for rlpark



# Troubleshooting #

First things first, make sure everything is plugged in, this means that when you apply power to the arm, the small red LEDs on the servos blink.

### If the red LED in the dynamixel2USB converter does not turn on###
It could be that it isn't getting enough power through the USB that it is attached to. Using a USB hub with a separate power adapter should solve this issue.

### Cannot find controller module. Unable to start controller multi_mode_dynamixel_controller. No module named single_joint_controller ###

Presently the Bento software is dependent on a custom fork for the dynamixel code (bad Craig). Clone it into your ROS workspace. https://github.com/csherstan/dynamixel_motor

### Permission denied when trying to access /dev/ttyUSB0 ###

The user must be added to the dialout user group, the following command should work (this change requires a reboot):

```
#!bash

sudo adduser $USER dialout

```
####  No such file or directory: '/dev/ttyUSB0/' ####

The first check here is to see if the bento arm is indeed on the correct port: This can be done by using the Device to ID snippet available in the adaptive prosthetics group.
If it is determined that the port is different, going into bento/launch/controller_manager.launch and editing the port name there to the correct port for the set up should do the trick.

Else, if the problem persists after checking that the bento is actually in port /dev/ttyUSB0/, go into dynamixel_driver/scripts and run info_dump.py for different joint ids and baud rates:

For example, testing ID 3 for baud 115200 would look like:

    ./info_dump.py 3 --baud=115200

Once you know what the baud rates of the servos are, you can change them with:

    ./set_servo_config.py --baud=115200 -r 1 1 2 3 4    


Where the number after -r corresponds to the Data column of the table:

| Data | Set BPS | Target BPS | Tolerance |
|------|---------|------------|-----------|
| 1    | 1000000 | 1000000    | 0         |
| 3    | 500000  | 500000     | 0         |
| 4    | 400000  | 400000     | 0         |
| 7    | 250000  | 250000     | 0         |
| 9    | 200000  | 200000     | 0         |
| 16   | 117647.1| 115200     | -2.124%   |
| 34   | 57142.9 | 57600      | 0.794%    |
| 103  | 19230.8 | 19200      | -0.160%   |
| 207  | 9615.4  | 9600       | -0.160%   |

This table comes from the eManual (http://support.robotis.com/en/)


### Catkin_make fails for joystick_drivers - Missing "usb.h" and "spnav.h" ####

To install the package usb.h , do:
```
#!bash
sudo apt-get install libusb-dev
```
To install the package spnav.h, do:
```
#!bash
sudo apt-get install libspnav-dev
```
