#!/usr/bin/env python

import argparse
import rospy
from std_msgs.msg import Header, UInt8
from adc_msgs.msg import ADCSignal
from ros_myo.msg import EmgArray
from bento_controller.srv import ToggleGroup


########################################################################
class BentoMyo(object):
    """"""

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""
        rospy.init_node("BentoMyo", anonymous=True)
        rospy.Subscriber("/myo_emg", EmgArray, self.emg_callback)
        rospy.Subscriber("/myo_gest", UInt8, self.gest_callback)
        self.adc_pub = rospy.Publisher("/adc", ADCSignal, queue_size=100)
        self.toggle_proxy = rospy.ServiceProxy('/bento/toggle_group', ToggleGroup)
        
        self.last_gest = 0
        
    def emg_callback(self, msg):
        if self.last_gest in [2,3]:
            signal = [d for d in msg.data]
            self.adc_pub.publish(header=Header(stamp=rospy.Time().now()), signal = signal)
        else:
            signal = [0.0 for d in msg.data]
            self.adc_pub.publish(header=Header(stamp=rospy.Time().now()), signal = signal)
            
        
    def gest_callback(self, msg):
        # hardcoded to a co-contraction
        target_gest = [1,4]
        if msg.data in target_gest and self.last_gest not in target_gest:
            self.toggle_proxy(group_idx = 0, backward = False)
        self.last_gest = msg.data
        
if __name__=="__main__":
    
    try:
        BentoMyo()
        rospy.spin()
    except rospy.ROSInterruptException: pass