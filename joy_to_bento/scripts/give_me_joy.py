#!/usr/bin/env python

from bento_controller.msg import *
from bento_controller.srv import *
from joy_to_bento.mappings import *
from joy_to_bento.controllers import *
import numpy as np
import operator
import rospy
from sensor_msgs.msg import Joy
from std_msgs.msg import String



class JoyToBento(object):
  """
  This is the main class for handling interaction of Joystick commands to control the Bento arm.
  Configuration allows for control of individual joints or joint groups
  """
  ros_prefix = "joy_to_bento"
  
  def __init__(self):
    # command and group_command publishers
    self.publisher = rospy.Publisher("/bento/command", BentoCommand, queue_size=50)
    self.group_command_publisher = rospy.Publisher("/bento/group/command", BentoGroupCommand, queue_size=1)

    # bento service calls
    self.pause_srv = rospy.ServiceProxy("/bento/pause", Pause)
    self.torque_srv = rospy.ServiceProxy("/bento/torque_enable", TorqueEnable)
    self.go_home_srv = rospy.ServiceProxy("/bento/go_home", GoHome)
    self.is_paused_srv = rospy.ServiceProxy("/bento/is_paused", IsPaused)
    self.toggle_srv = rospy.ServiceProxy("/bento/toggle_group", ToggleGroup)
    
    # initialized paused state
    self.is_paused = BoolWrapper(self.is_paused_srv.call().state)    
    self.torque_enabled = BoolWrapper(True)
    # Set False if should not be sending command signals, buttons remain active, just won't sent Command or GroupCommands
    self.commanding = BoolWrapper(True)    
    
    self.configure(rospy.get_param(self.ros_prefix))    
    
    # listen for joy messages
    rospy.Subscriber(name = "/joy", data_class = Joy, callback=self.got_joy)
    rospy.Subscriber("/bento/state", BentoState, self.bento_callback)    

  def configure(self, config):
    self.config = config
    
    assert self.config.has_key("controller")
      
    # support loading a custom mapping
    if type(self.config["controller"]) is dict:
      self.controller = self.config["controller"]
    else:    
      self.controller = ControllerMaps.get_map(self.config["controller"])
      
    # load mappings
    self.interrupts = [] #Needs to be ordered and as soon as one fires we abort
    self.first_priority = []
    self.second_priority = []
    
    for mapping in self.config["mappings"]:
      handler = None
      action = mapping["action"]
      if action == "go_home":
        self.interrupts.append({"priority": 0, "mapping": GoHomeMapping(mapping, self.controller, self.go_home_srv)})
      elif action == "pause":
        self.interrupts.append({"priority": 1, "mapping": PauseMapping(mapping, self.controller, self.pause_srv, self.is_paused)})
      elif action == "torque":
        self.interrupts.append({"priority": 2, "mapping": TorqueMapping(mapping, self.controller, self.torque_srv, self.torque_enabled)})
      elif action == "toggle":
        self.first_priority.append(ToggleMapping(mapping, self.controller, self.toggle_srv))
      elif action == "command":
        self.first_priority.append(CommandMapping(mapping, self.controller, self.publisher, self.commanding))
      elif action == "group_command":
        self.second_priority.append(GroupCommandMapping(mapping,
                                                        self.controller, self.group_command_publisher, self.commanding))
      elif action == "toggle_active":
        self.interrupts.append({"priority": 3, "mapping": ToggleActive(mapping, self.controller, self.commanding)})
        
    # sort interrupts
    self.interrupts = sorted(self.interrupts, key=operator.itemgetter("priority"))
        
  def bento_callback(self, msg):
      self.is_paused.state = msg.paused
      self.torque_enabled.state = msg.torque
      
  def got_joy(self, msg):
    """
    joy callback handler, processes messages sends joint and service commands
    """        
    for interrupt in self.interrupts:
      if interrupt["mapping"].handle_msg(msg):
        break
    # gets called if break is never called
    else:
      for mapping in self.first_priority:
        mapping.handle_msg(msg)
      for mapping in self.second_priority:
        mapping.handle_msg(msg)
    
if __name__ == '__main__':
  try:
    rospy.init_node('joy_to_bento', anonymous=True)
    rospy.wait_for_service('/bento/enumerate')
    JoyToBento()
    rospy.spin()
  except rospy.ROSInterruptException: pass
    