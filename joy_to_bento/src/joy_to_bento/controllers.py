#!/usr/bin/env python

import rospy

class ControllerMaps(object):
  # mappings for various controllers between named controls and indices
  xbox_wired = {
    "axes": {
      "left-x": 0,
      "left-y": 1,
      "left-trigger": 2,
      "right-x": 3,
      "right-y": 4,
      "right-trigger": 5,
      "cross-x": 6,
      "cross-y": 7
    },
    "buttons": {
      "a": 0,
      "b": 1,
      "x": 2,
      "y": 3,
      "lb": 4,
      "rb": 5,
      "back": 6,
      "start": 7,
      "power": 8,
      "left-click": 9,
      "right-click": 10
    }
  }
  
  # Wireless has slightly different mapping  
  xbox_wireless = {
    "axes": {
      "left-x": 0,
      "left-y": 1,
      "right-x": 2,
      "right-y": 3,
      "left-trigger": 4,
      "right-trigger": 5,
      "cross-x": 6,
      "cross-y": 7
    },
    "buttons": {
      "a": 0,
      "b": 1,
      "x": 2,
      "y": 3,
      "lb": 4,
      "rb": 5,
      "back": 6,
      "start": 7,
      "power": 8,
      "left-click": 9,
      "right-click": 10
    }
  }
  
  sixaxis = {
    "axes": {
      "left-x": 0,
      "left-y": 1,
      "right-x": 2,
      "right-y": 3,
      "d-left": 11,
      "d-right": 9,
      "d-up": 8,
      "d-down": 10,
      "l-shoulder": 14,
      "l-trigger": 12,
      "r-shoulder": 15,
      "r-trigger": 13,
      "triangle": 16,
      "circle": 17,
      "x": 18,
      "square": 19
    },
    "buttons": {
      "d-left": 7,
      "d-up": 4,
      "d-right": 5,
      "d-down": 6,
      "l-shoulder": 10,
      "l-trigger": 8,
      "r-shoulder": 11,
      "r-trigger": 9,
      "left-click": 1,
      "right-click": 2,
      "select": 0,
      "start": 3,
      "ps": 16,
      "triangle": 12,
      "circle": 13,
      "x": 14,
      "square": 15
    }
  }
  
  predefined = {"xbox_wired": xbox_wired, "xbox_wireless": xbox_wireless, "sixaxis": sixaxis}
  
  def __init__(self):
    pass
  
  @staticmethod
  def get_map(name):
    assert name in ControllerMaps.predefined
    return ControllerMaps.predefined[name]
  
  @staticmethod
  def lookup_button(controller_map, button_name):
    return controller_map["buttons"].get(button_name.lower())
  
  @staticmethod
  def lookup_axis(controller_map, axis_name):
    return controller_map["axes"].get(axis_name.lower())

class Control(object):
  def __init__(self, mapping, controller):
    self.mapping = mapping
    self.validate()
  def handle_msg(self, msg):
    raise NotImplementedError
  def get_threshold(self):
    return self.mapping.get("threshold", 0.0)
  def get_flip(self):
    return self.mapping.get("flip", False)
  def get_offset(self):
    return self.mapping.get("offset", False)
  def validate(self):
    pass

class AxisControl(Control):
  def __init__(self, mapping, controller, name):
    Control.__init__(self, mapping, controller)
    self.name = name
    self.axis = ControllerMaps.lookup_axis(controller, name)
  def handle_msg(self, msg):
    return self.calculate_velocity(msg, self.axis, self.get_threshold(), self.get_flip(), self.get_offset())
  
  def calculate_velocity(self, msg, axis, threshold, flip, offset):
    val = msg.axes[axis]    
    if abs(val) < threshold:
      val = 0
    else:
      if flip:
        val *=-1
      if offset:
        val += 1
        val /= 2
      
    return val
    
    
########################################################################
class ButtonControl(Control):
  """"""

  #----------------------------------------------------------------------
  def __init__(self, mapping, controller, name):
    """Constructor"""
    Control.__init__(self, mapping, controller)
    self.name = name
    self.button_idx = ControllerMaps.lookup_button(controller, name)
  def handle_msg(self, msg):
    val = msg.buttons[self.button_idx]
    return val
  
########################################################################
class OpposedControl(AxisControl):
  """"""

  #----------------------------------------------------------------------
  def __init__(self, mapping, controller, name_list):
    """Constructor"""
    Control.__init__(self, mapping, controller)
    assert len(name_list) == 2
    self.name_list = name_list
    self.axis_0 = ControllerMaps.lookup_axis(controller, name_list[0])
    self.axis_1 = ControllerMaps.lookup_axis(controller, name_list[1])
    
  def handle_msg(self, msg):
    thresholds = self.get_threshold()
    flip = self.get_flip()
    offset = self.get_offset()
    
    val_0 = self.calculate_velocity(msg, self.axis_0,
                                     thresholds[0], flip[0], offset[0])
    val_1 = self.calculate_velocity(msg, self.axis_1, thresholds[1], flip[1],
                                    offset[1])
    return val_0 - val_1
  
  def get_flip(self):
    return self.mapping.get("flip", [False, False])
  
  def get_offset(self):
    return self.mapping.get("offset", [False, False])  
  
  def get_threshold(self):
    return self.mapping.get("threshold", [0.0, 0.0])
  
  def validate(self):
    offset = self.get_offset()
    assert isinstance(offset, list)
    assert len(offset) == 2
    
    flip = self.get_flip()
    assert isinstance(flip, list)
    assert len(flip) == 2
    
    threshold = self.get_threshold()
    assert isinstance(threshold, list)
    assert len(threshold) == 2