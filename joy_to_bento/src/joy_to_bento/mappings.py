#!/usr/bin/env python

from bento_controller.msg import *
from bento_controller.srv import *
from controllers import *
import math
import numpy as np
from std_msgs.msg import Header

class BoolWrapper(object):
  def __init__(self, state):
    self.state = state

class Mapping(object):
  def __init__(self, mapping, controller):
    self.definition = mapping
    self.validate(mapping)
    self.control = self._make_control(mapping, controller)
  def get_config(self):
    return self.definition["config"]    
  def _handle_val(self, val):
    raise NotImplementedError
  def handle_msg(self, msg):
    """
      return True to indicate if action was taken on the message
    """
    return self._handle_val(self.control.handle_msg(msg))
  def validate(self, mapping):
    raise NotImplementedError
  
  def _make_control(self, mapping, controller):
    _control = {type: None}
    control = mapping["control"]
    if type(control) is list:
      assert len(control) == 2
      _control = OpposedControl(mapping, controller, control)
    elif ControllerMaps.lookup_axis(controller, control) is not None:
      _control = AxisControl(mapping, controller, control)
    elif ControllerMaps.lookup_button(controller, control) is not None:
      _control = ButtonControl(mapping, controller, control)
    return _control
  
class ToggleActive(Mapping):
  def __init__(self, mapping, controller, active_wrapper):
    Mapping.__init__(self, mapping, controller)
    
    self.active_wrapper = active_wrapper
    
    self.last_val = 0
    
  def _handle_val(self, val):
    ret = False
    if val > 0:
      ret = True
      
      if not self.last_val:
        self.active_wrapper.state = not self.active_wrapper.state
      
    self.last_val = val
    
    return ret
  def validate(self, mapping):
    # TODO: implement
    pass
  
class GoHomeMapping(Mapping):
  def __init__(self, mapping, controller, go_home_srv):
    Mapping.__init__(self, mapping, controller)
    self.go_home_srv = go_home_srv
    
    self.last_val = 0
    
  def _handle_val(self, val):
    ret = False
    if val > 0:
      ret = True
      
      if not self.last_val:
        self.go_home_srv.call()
      
    self.last_val = val
    
    return ret
  
  def validate(self, mapping):
    # TODO: implement
    pass
      
class PauseMapping(Mapping):
  def __init__(self, mapping, controller, pause_srv, pause_state):
    Mapping.__init__(self, mapping, controller)
    self.pause_srv = pause_srv
    
    # pause_state is potentially shared among different holders
    # expected to be a BoolWrapper
    assert isinstance(pause_state, BoolWrapper)
    self.pause_state = pause_state
    self.last_val = 0
    
  def _handle_val(self, val):
    ret = False
    if val > 0:
      ret = True
      
      #this check prevents us from toggling back and forth, on press equals one action
      if not self.last_val:
        resp = self.pause_srv(state = (not self.pause_state.state))
        self.pause_state.state = resp.state

    self.last_val = val
    
    return ret
      
  def validate(self, mapping):
    # TODO: implement
    pass
      
class TorqueMapping(Mapping):
  def __init__(self, mapping, controller, torque_srv, torque_state):
    Mapping.__init__(self, mapping, controller)
    self.torque_srv = torque_srv
    assert isinstance(torque_state, BoolWrapper)
    self.torque_state = torque_state
    self.last_val = 0
    
  def _handle_val(self, val):
    ret = False
    if val > 0:
      ret = True
      
      #this check prevents us from toggling back and forth, on press equals one action
      if not self.last_val:
        self.torque_state.state = not self.torque_state.state
        self.torque_srv.call(enable=self.torque_state.state)

    self.last_val = val
    
    return ret
      
  def validate(self, mapping):
    # TODO: implement
    pass      
      
class ToggleMapping(Mapping):
  def __init__(self, mapping, controller, toggle_srv):
    Mapping.__init__(self, mapping, controller)
    self.toggle_srv = toggle_srv
    self.last_val = 0
    
  def _handle_val(self, val):
    ret = False
    if val > 0 and self.last_val == 0:
      self.toggle_srv.call(group_idx = self.get_group_idx(), backward = self.get_backward())
      ret = True
    self.last_val = val
    
    return True
      
  def get_backward(self):
    return self.get_config().get("backward", False)
  
  def get_group_idx(self):
    return self.get_config()["group_idx"]
  
  def validate(self, mapping):
    # TODO: implement
    pass  
      
class CommandMapping(Mapping):  
  def __init__(self, mapping, controller, publisher, active):
    Mapping.__init__(self, mapping, controller)
    self.publisher = publisher
    self.last_val = None
    
    # This is a bool wrapper. When True we can send values otherwise should not send anything
    self.active = active
    
    
  def _handle_val(self, val):
    if not self.active.state:
      return False
    
    ret = False
    
    velocity = 0.0
    
    if self.has_changed(val):
      ret = True
      if abs(val) > 0:
        velocity = (self.get_max_speed()-self.get_min_speed())*abs(val) + self.get_min_speed()
        velocity *= math.copysign(1, val)
        if self.get_inverted():
          velocity *= -1.0
      
      self.publisher.publish(header=Header(stamp=rospy.Time().now()), joint_commands = [JointCommand(type="velocity",
                                                            id=self.get_joint_id(), velocity=velocity)])
      self.last_val = val
      
    return ret
    
  def has_changed(self, val):
    return True
    #This is probably not necessary
    #thresh = 0.00001
    #return self.last_val is None or abs(val - self.last_val) > thresh or\
           #np.sign(val) != np.sign(self.last_val)
    
  def get_joint_id(self):
    return self.get_config()["joint_id"]
  def get_max_speed(self):
    return self.get_config().get("max_speed", 1.0)
  def get_min_speed(self):
    return self.get_config().get("min_speed", 0.0)
  def get_inverted(self):
    return self.get_config().get("inverted", False)
  
  def validate(self, mapping):
    # TODO: implement
    pass  
    
class GroupCommandMapping(Mapping):
  def __init__(self, mapping, controller, publisher, active):
    Mapping.__init__(self, mapping, controller)
    self.publisher = publisher
    self.last_val = None
    
    # BoolWrapper, if False, should not send a command
    self.active = active
    
  def _handle_val(self, val):
    if not self.active.state:
      return False
    
    ret = False
    
    if self.has_changed(val):
      self.publisher.publish(header=Header(stamp=rospy.Time().now()), commands =
                             [GroupCommand(group_idx=self.get_group_idx(),velocity=val)])
      ret = True
      
      self.last_val = val
      
    return ret
      
  def has_changed(self, val):
    return True
    # This was causing a problem where every time you toggled you had
    # to move the joystick again for it to move the newly selected joint
    #thresh = 0.00001
    #return self.last_val is None or abs(val - self.last_val) > thresh or\
           #np.sign(val) != np.sign(self.last_val)
  
  def get_group_idx(self):
    return self.get_config()["group_idx"]
  
  def validate(self, mapping):
    # TODO: implement
    pass  