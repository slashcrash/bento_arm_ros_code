#!/usr/bin/env python

import argparse
import rospy
from bento_controller.msg import *
from std_msgs.msg import Header


########################################################################
class Kinect_to_bento(object):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, topic):
        """Constructor"""
        rospy.init_node("kinect_to_bento", anonymous=True)
        rospy.Subscriber(topic, bento_controller.msg.BentoCommand, self.pose_callback)
        self.command_pub = rospy.Publisher("/%s/command" % self.ros_prefix, bento_controller.msg.BentoCommand)
        
    def pose_callback(self, msg):
        
        position = []
        self.command_pub.publish(header=Header(stamp=rospy.Time().now()), joint_commands = [JointCommand(type="position",
                                                            id=self.get_joint_id(), position=position)])
        
    
    


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="description")
    parser.add_argument('topic', type=str, help='')
    args = parser.parse_args()

    
    
    
    try:
        Kinect_to_bento(args.topic)
        rospy.spin()
    except rospy.ROSInterruptException: pass