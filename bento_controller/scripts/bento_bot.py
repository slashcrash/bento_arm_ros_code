#!/usr/bin/env python
__author__ = 'Craig Sherstan'

import rospy
import sensor_msgs.msg
from std_msgs.msg import Header
from std_msgs.msg import String
import bento_controller.msg
import bento_controller.srv


import argparse
from dynamixel_controllers.srv import StartController
from dynamixel_controllers.srv import StopController
from dynamixel_controllers.srv import RestartController
from dynamixel_msgs.msg import MotorStateList

import bento_controller.joints as joints
import math
from bento_controller.joint_group import JointGroup

import numpy as np

class BentoBot(object):
    # Ok, so we're saying that all communication should go through this controller.
    # It will need to take in some sort of control msgs and publish some state messages.
    # It will need to coordinate the interaction of the different controllers.
    # It will need to handle error conditions (temperature and load on the joints).
    
    # TODO: I do not like that the grouping functionality was rolled into BentoBot. Would prefer to extract it to it's own node.
    
    # prefix used in topics
    ros_prefix = "bento"

    def __init__(self):
        rospy.init_node('bento', anonymous=True)
        self.subscribers = []
        
        self.config = rospy.get_param("bento_controller")

        self.over_temperature = False
        self.paused = rospy.get_param("~wait", True)
        self.torque = True
        self.meta = None
        # TODO: this is not the right way to do this as the controller may be removed without us knowing about it
        self.joints = {}     

        self.manager_namespace = rospy.get_param("~manager")
        self.port_namespace = rospy.get_param("~port")
        self.update_rate = rospy.get_param("~rate", 30)
        self.initialize_manager_services()
        
        self.initialize_joints()

        # Not actually used at this point
        self.max_temperature = rospy.get_param("~max_temperature", 60)        

        self.joint_groups = []
        if rospy.has_param("/bento_controller/joint_groups"):
            self.configure_groups(rospy.get_param("/bento_controller/joint_groups"))

        self.subscribers.append(rospy.Subscriber("/%s/command" % self.ros_prefix, bento_controller.msg.BentoCommand, self._command_callback))
        self.subscribers.append(rospy.Subscriber("/%s/group/command" % self.ros_prefix, bento_controller.msg.BentoGroupCommand, self._group_command_callback))

        # Publishers        
        self.state_publisher = rospy.Publisher("/%s/state" % self.ros_prefix, bento_controller.msg.BentoState, queue_size=10)
        # This one is for RVIZ
        self.joint_state_publisher = rospy.Publisher("/%s/joint_states" % self.ros_prefix, sensor_msgs.msg.JointState, queue_size=10)
        self.select_joint_publisher = rospy.Publisher("/%s/selected_joint" % self.ros_prefix, bento_controller.msg.JointSelection, queue_size=10)
        self.meta_publisher = rospy.Publisher("/%s/meta" % self.ros_prefix, bento_controller.msg.Meta, queue_size=10)
        self.joint_group_update_publisher = rospy.Publisher("/%s/joint_group_update" % self.ros_prefix, bento_controller.msg.JointGroup, queue_size=10)

        # Services
        rospy.Service('/%s/pause' % self.ros_prefix, bento_controller.srv.Pause, self.pause)
        rospy.Service('/%s/is_paused' % self.ros_prefix, bento_controller.srv.IsPaused, self.is_paused)
        rospy.Service("/%s/enumerate" % self.ros_prefix, bento_controller.srv.Enumerate, self.enumerate_handler)
        rospy.Service("/%s/torque_enable" % self.ros_prefix, bento_controller.srv.TorqueEnable, self._torque_enable_handler)
        rospy.Service("/%s/go_home" % self.ros_prefix, bento_controller.srv.GoHome, self._go_home_handler)

        rospy.Service("/%s/toggle_group" % self.ros_prefix, bento_controller.srv.ToggleGroup, self._toggle_group_handler)
        rospy.Service("/%s/select_joint" % self.ros_prefix, bento_controller.srv.SelectJoint, self._select_joint_handler)
        rospy.Service("/%s/configure_group" % self.ros_prefix, bento_controller.srv.ConfigureGroup, self._configure_group_handler)
        
        rospy.Service('/%s/set_meta' % self.ros_prefix, bento_controller.srv.SetMeta, self._set_meta_handler)
        
        # start our state_callback timer
        rospy.Timer(rospy.Duration(1.0/self.update_rate), self.state_timer_callback)

    def _add_dynamixel(self, joint_def, joint_state):
        """
        creates a joint with type=="dynamixel"
        """
        name = "bento_controller/dynamixel/%s" % joint_def["joint_name"]
        rospy.set_param("/%s" % name, joint_def)
        joint = None
        try:
            response = self.start_controller(self.port_namespace,
                                             "bento_dynamixel",
                                             "multi_mode_dynamixel_controller",
                                             "MultiModeDynamixelController", name, [])
            if response.success: 
                rospy.loginfo(response.reason)
                joint = joints.DynamixelJoint(joint_def["motor"]["id"], joint_def, joint_state, name)
            else: rospy.logerr(response.reason)
        except rospy.ServiceException, e:
            rospy.logerr('Service call failed: %s' % e)

        return joint    

    def _all_stop(self):    
        """
        Stops all joints by setting the velocity to 0.
        """
        for idx, joint in self.joints.items():
            joint.stop()

    def __exit__(self, exception_type, exception_val, trace):
        self.close()

    def __del__(self):
        self.close()

    def close(self):
        for sub in self.subscribers:
            sub.unregister()
            
    def get_group(self, group_idx):
        assert group_idx >= 0 and group_idx < len(self.joint_groups)
        return self.joint_groups[group_idx]
    
    def get_joint_by_id(self, joint_id):
        return self.joints[joint_id]
            
    def _create_joint_msg(self, joint):
        return bento_controller.msg.Joint(name = joint.get_joint_name(), nice_name =
                     joint.get_nice_name(), id=joint.get_joint_id())

    def enumerate_handler(self, req):
        """
        Handles the service call to enumerate the Bento controller.
        This returns a list of the joints.
        """
        joints = [self._create_joint_msg(joint) for joint_id, joint in self.joints.items()]
        joint_groups = [group.enumerate(group_idx) for group_idx, group in enumerate(self.joint_groups)]
        
        return bento_controller.srv.EnumerateResponse(joints = joints, joint_groups =
                                 joint_groups)

    """
  The following getters are probably overkill. The idea was that if I use these I could have flexibility
  to change the config file as I saw fit, while only making a single change in the code.
  I have run into situations where this is useful, but in general it's probably not necessary
  """
    @staticmethod
    def get_joint_name_from_def(joint_def):
        return joint_def["joint_name"]
    
    @staticmethod
    def get_nice_name_from_def(joint_def):
        return joint_def["nice_name"]

    @staticmethod
    def get_type_from_def(joint_def):
        return joint_def["type"]

    @staticmethod
    def get_joint_id_from_def(joint_def):
        return joint_def["joint_id"]
    
    def initialize_manager_services(self):
        """
        sets up all services for the main dynamixel port controller
        """    
        start_service_name = '%s/%s/start_controller' % (self.manager_namespace, self.port_namespace)
        stop_service_name = '%s/%s/stop_controller' % (self.manager_namespace, self.port_namespace)
        restart_service_name = '%s/%s/restart_controller' % (self.manager_namespace, self.port_namespace)

        rospy.wait_for_service(start_service_name)
        rospy.wait_for_service(stop_service_name)
        rospy.wait_for_service(restart_service_name)        

        parent_namespace = 'global' if rospy.get_namespace() == '/' else rospy.get_namespace()
        rospy.loginfo('%s controller_spawner: waiting for controller_manager %s to startup in %s namespace...' % \
                      (self.port_namespace, self.manager_namespace, parent_namespace))                

        self.start_controller = rospy.ServiceProxy(start_service_name, StartController)
        self.stop_controller = rospy.ServiceProxy(stop_service_name, StopController)
        self.restart_controller = rospy.ServiceProxy(restart_service_name, RestartController)

    def initialize_joints(self):
        """
        Handles initializing each joint. For each joint multiple controllers are created corresponding to each type of
        joint control to be used (velocity, position, torque). I don't really like this method of having multiple controllers
        handling each joint.
        """
        
        num_joints = len(self.config["joints"])
        
        self.joint_state_buffer = [bento_controller.msg.JointState() for i in xrange(num_joints)]
        
        for joint_idx, joint_def in enumerate(self.config["joints"]):            
            joint_state = self.joint_state_buffer[joint_idx]
            if BentoBot._validate_joint_def(joint_def):
                new_joint = None
                if BentoBot.get_type_from_def(joint_def).lower() == "dynamixel":
                    new_joint = self._add_dynamixel(joint_def, joint_state)
                else:
                    raise NotImplementedError

                if new_joint:
                    self.joints[BentoBot.get_joint_id_from_def(joint_def)] = new_joint
                    
        def motor_state_length_checker(msg):
            # TODO: in actuality num_joints may not equal the number of motors
            if len(msg.motor_states) != num_joints:
                rospy.logerr("motor_states length is not correct: %d" % len(msg.motor_states))
        self.subscribers.append(rospy.Subscriber("/motor_states/%s" %
                                                 self.port_namespace, MotorStateList, motor_state_length_checker))
                    
    def _go_home_handler(self, req):
        """
        Handles service request to go to home position. GoHome should probably be replaced with an ActionLib
        """

        # loop through each joint, sets a slow speed then moves to pi position (this is the middle where of the joint motion as
        # they are currently assembled).
        self._pause(True)
        rospy.loginfo("Going Home")
        speed = 0.5
        
        for id_, joint in self.joints.items():
            if not req.apply_to or id_ in req.apply_to:
                joint.go_home(speed)
            
        self.torque = True

        return bento_controller.srv.GoHomeResponse()
    
    def _set_meta_handler(self, msg):
        if self.meta != msg.meta:
            self.meta = msg.meta
            self.meta_publisher.publish(header=Header(stamp=rospy.Time().now()), meta = self.meta)
        return {}

    def state_timer_callback(self, timer_event):
        header = Header()
        header.stamp = rospy.Time.now()
        
        group_states = [bento_controller.msg.JointSelection(joint_group=idx,
                                                            joint_idx=joint_group.current,
                                                            joint_id=joint_group.get_current_joint_id()) for idx, joint_group in
                        enumerate(self.joint_groups)]
        
        self.state_publisher.publish(header = header, joint_states =
                                     self.joint_state_buffer, paused = self.paused, torque = self.torque, meta =
                                                      self.meta, group_states=group_states)
        self.pub_joint_state()
        
    def convert_encoder_to_rads(self, motor_id, encoder):
        res = rospy.get_param("dynamixel/%s/%d/encoder_resolution" % (self.port_namespace, motor_id))
        return 2*np.pi*(float(encoder)/(res - 1))
        
    def get_joint_by_dynamixel_id(self, id):
        """
        Presently the dynamixel_id and joint_id are the same thing. This will likely change.
        """
        return self.get_joint_by_id(id)
    
    def pub_joint_state(self):
        """
        Publishes a JointState message for use with RVIZ
        """
        header = Header()
        header.stamp = rospy.Time.now()
        
        name = []
        position = []
        velocity = []
        
        for id, joint in self.joints.iteritems():
            name.append(joint.get_rviz_name())
            joint_state = joint.get_joint_state()
            position.append(joint_state.current_pos)
            velocity.append(joint_state.velocity)
            
        self.joint_state_publisher.publish(header = header,  name=name, position=position, velocity=velocity, effort=[])

    def _command_callback(self, msg):
        """
        Handles a command to move
        """
        # only process if we are actually accepting commands at the moment
        if(self._is_accepting_commands()):
            self._validate_command(msg)     

            # loop through all the joint commands, could be one or many
            for idx, command in enumerate(msg.joint_commands):
                joint_id = command.id                
                if(command.id in self.joints):
                    joint = self.joints[command.id]
                    command_type = command.type
                    if(command_type == 'velocity'):
                        joint.handle_velocity_command(command)
                    elif(command_type == 'position'):
                        joint.handle_position_command(command)
                    elif(command_type == 'position_and_velocity'):
                        joint.handle_position_and_velocity_command(command)
                    elif(command_type == 'torque'):
                        rospy.logerr('torque not implemented')
                    else:
                        rospy.logerr('Invalid JointCommand: %s' % command_type)
                else:
                    rospy.logerr('Invalid Joint Id: %d' % command.id)

            # TODO: want to ensure that commands are always ordered in ascending joint id order

    def _is_accepting_commands(self):
        """
        Returns true if the arm is accepting commands, false otherwise.
        Will accept commands if not paused and not over temperature
        """
        return not self.paused and not self.over_temperature

    def is_paused(self, data):
        """
        returns:
          paused - True if paused
        """
        return bento_controller.srv.IsPausedResponse(state = self.paused)

    def _pause(self, pause_state):
        """
        Handler for a service call to pause the arm
        """
        self.paused = pause_state
        self._all_stop();
        rospy.loginfo("Paused: %r" % self.paused)
    
    def pause(self, data):
        """
        Handler for a service call to pause the arm
        """
        self._pause(data.state)
        return bento_controller.srv.PauseResponse(state = self.paused)

    def _shutdown_arm(self):
        """
        Shuts down the arm
        """
        # it looks like the best option is just to disable torque
        # perhaps we need to also tell it to set the desired position at the current position
        # TODO: set torque very low, but don't disable.
        pass

    def _torque_enable_handler(self, req):
        """
        Handles turning torque on and off for the whole arm    
        """
        #self._pause(True)
        self.paused = True
        
        if not req.apply_to:
            self.torque = req.enable
            
        for id_, joint in self.joints.items():
            if not req.apply_to or id_ in req.apply_to:
                joint.torque_enable(req.enable)
            
        rospy.loginfo("Torque: %r" % self.torque)

        return bento_controller.srv.TorqueEnableResponse()

    def _validate_command(self, msg):
        """
        Validates the command received. At present does nothing.
        """
        # ensure we are not moving outside safe boundaries.
        pass

    @staticmethod
    def _validate_joint_def(joint_def):
        """
        validates the joint definition
        """
        assert joint_def.has_key("type")
        # TODO: implement
        return True
    
#---------- GROUPING STUFF ----------------
    def configure_groups(self, config):
        """
        Simply creates all the JointGroup objects for all groups described in the
        config
        """
        self.joint_groups = []
        for idx, joint_group in enumerate(config):
            self.joint_groups.append(JointGroup(config = joint_group))
            
    def configure_group(self, group_idx, config, reset_idx=False):
        assert group_idx >=0 and group_idx < len(self.joint_groups)
        
        if self.joint_groups[group_idx]:
            self.joint_groups[group_idx].update_config(config, reset_idx)
        else:
            self.joint_groups[group_idx] = JointGroup(config = config)
        
    def _configure_group_handler(self, req):
        """
        This is a bit of an experiment. Passing config objects through a service call is cumbersome, because
        of their complex structure, which would require you to build different message types for each layer.
        So I'm going to instead pass everything around as JSON.        
        """
        self.configure_group(req.group_idx, {"name": req.name, "joints":
                                             [{"id": j.joint_id, "min_speed": j.min_speed, "max_speed":
                                               j.max_speed} for j in req.joints]}, req.reset_idx)
        group = self.get_joint_group(req.group_idx).enumerate(req.group_idx)
        self.joint_group_update_publisher.publish(group)
        return {"group": group}
        

    def _group_command_callback(self, cmd):
        """
        ROS call back for a BentoGroupCommand.msg
        """
        
        if(self._is_accepting_commands()): 
            for command in cmd.commands:
                group_idx = command.group_idx
                group = self.get_joint_group(group_idx)
                if group:
                    joint_id = group.get_current_joint_id()
                    joint = self.joints[joint_id]
                    joint.handle_velocity_command(group.make_joint_command(command.velocity))
                
    def _get_current_joint(self, group_idx):
        group = self.get_joint_group(group_idx)
        return self.joints[group.get_current_joint_id()]
            
    def get_joint_group(self, group_idx):
        """
        Helper function
        """
        return self.joint_groups[group_idx] if group_idx >= 0 and group_idx <\
               len( self.joint_groups) else None

    def _publish_joint_selection(self, group_idx):
        """
        Publishes the selection of joint for a JointGroup over ROS
        """
        joint_group = self.get_joint_group(group_idx)
        if joint_group:
            self.select_joint_publisher.publish(header=Header(stamp=rospy.Time().now()), joint_group = group_idx, joint_idx =
                                                joint_group.current, joint_id =
                                                joint_group.get_current_joint_id())

    def _select_joint_handler(self, req):
        """
        ROS handler for a SelectJoint.srv
        """
        group_idx = req.group_idx
        group = self.get_joint_group(group_idx)
        
        assert group

        # Validation is already handled in the JointGroup        
        group.current = req.joint_idx

        # let everyone know there has been a change
        self._publish_joint_selection(group_idx)

        return bento_controller.srv.SelectJointResponse(joint_id = group.get_current_joint_id(), 
                                   joint_idx = group.current)
            

    def _toggle_group_handler(self, req):
        """
        ROS handler for a ToggleGroup.srv
        """
        
        joint_group = self.get_joint_group(req.group_idx)
        
        if not joint_group:
            raise rospy.ServiceException("Group %d does not exist" % req.group_idx)
        
        # Stop the currently selected joint before toggling
        # to the next
        joint = self._get_current_joint(req.group_idx)
        # Not sure why stopping isn't working properly, but I get drift in the previously
        # selected joint when I am moving and switch.
        joint.stop()
        #command = bento_controller.msg.JointCommand(type="velocity", velocity=0.0)
        #joint.handle_velocity_command(command)
        
        joint_group.toggle(req.backward)
        
        # let everyone know that the joint selection for the group has changed
        self._publish_joint_selection(req.group_idx)
        
        return bento_controller.srv.ToggleGroupResponse(joint_idx = joint_group.current, joint_id
                                   = joint_group.get_current_joint_id())
if __name__ == '__main__':
    try:
        BentoBot()
        rospy.spin()
    except rospy.ROSInterruptException: pass