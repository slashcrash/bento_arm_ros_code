import rospy
from std_msgs.msg import String
from bento_controller.msg import BentoCommand
from bento_controller.msg import JointCommand
import time

if __name__ == '__main__':    
    rospy.init_node('bento_controller_test')
    pub = rospy.Publisher('/bento/command', BentoCommand)
        
    commands = [BentoCommand([JointCommand(type=String('position'), id=2, position=0)]),
                BentoCommand([JointCommand(type=String('velocity'), id=1, velocity=-1)]),
                BentoCommand([JointCommand(type=String('position'), id=2, position=6), JointCommand(type=String('velocity'), id=1, velocity=1)])]
    
    while(1):
        for command in commands:
            pub.publish(command)
            time.sleep(1)