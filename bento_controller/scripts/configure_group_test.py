#!/bin/usr/env python

import rospy

from bento_controller.msg import *
from bento_controller.srv import *

import argparse

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('which', type=int, help='')
    parser.add_argument('--reset', dest="reset", action="store_true")
    parser.set_defaults(reset=False)
    args = parser.parse_args()
    
    joints1 = [JointGroupJoint(joint_id=2, min_speed=0, max_speed=1),
              JointGroupJoint(joint_id=1, min_speed=0, max_speed=1)]
    
    joints2 = [JointGroupJoint(joint_id=4, min_speed=0, max_speed=1),
              JointGroupJoint(joint_id=1, min_speed=0, max_speed=1),
              JointGroupJoint(joint_id=3, min_speed=0, max_speed=1)]    
    
    joints = joints1
    if args.which == 2:
        joints = joints2
    
    prx = rospy.ServiceProxy("/bento/configure_group", ConfigureGroup)
    prx.call(group_idx=0, joints=joints, reset_idx=args.reset)