import bento_controller
import math

class JointGroup(object):
    """
    Encapsulates a single Joint Group.
    Note, must inherit from object for properties (@property, @current.setter) to work.
    Should always inherit all classes from object
    """
    def __init__(self, config):
        self._current = 0
        self.validate(config)
        self.config = config
        self.current_joint = self.config["joints"][self._current]
        
    @staticmethod
    def validate(config):
        if not config.has_key("name"):
            raise Exception("JointGroup missing name: %s" % config)
        
        def preamble(config):
            return "JointGroup %s: " % config["name"]
        
        if config.has_key("joints"):
            for joint in config["joints"]:
                if not joint.has_key("id"):
                    raise Exception(preamble(config) + "joint missing id")
                if not joint.has_key("min_speed"):
                    raise Exception(preamble(config) + "joint %d missing min_speed" % joint["id"])
                if not joint.has_key("max_speed"):
                    raise Exception(preamble(config) + "joint %d missing max_speed" % joint["id"])
        
    @property
    def current(self):
        return self._current
    
    @current.setter
    def current(self, joint_idx):
        if joint_idx >= self.get_joint_count() or joint_idx < 0:
            raise ValueError("joint_idx out of range: 0-%d" % (self.get_joint_count() - 1))
        else:
            self._current = joint_idx
            
        self.current_joint = self.config["joints"][self._current]
            
    def update_config(self, config, reset_idx=False):
        self.config = config
        if reset_idx or not self.get_joint(self.current_joint["id"]):
            self._current = -1
        
    def enumerate(self, group_idx):
        grouped_joints = [bento_controller.msg.JointGroupJoint(joint_id=joint["id"],
                                          min_speed=joint["min_speed"], max_speed=joint["max_speed"])for joint
                          in self.config["joints"]]
        return bento_controller.msg.JointGroup(name=self.get_name(), group_idx = group_idx, selected_joint_id = self.get_current_joint_id(), joints = grouped_joints)        
    
    def toggle(self, backward = False):
        """
        Change currently active joint in the group to the "next" joint.
        backward would move the toggle in the order of decreasing index
        """
        if backward:
            self._current = self._current - 1
            if self._current < 0:
                self._current = self.get_joint_count() - 1
        else:
            self._current = self._current + 1
            if self._current >= self.get_joint_count():
                self._current = 0
                
        self.current_joint = self.config["joints"][self._current]
                
        return self._current
    
    def get_joint(self, joint_id):
        return next((joint for joint in self.config["joints"] if joint["id"] == joint_id), None)
                
    def get_joint_count(self):
        """
        returns the number of joints in the group
        """
        return len(self.config["joints"])
    
    def get_current_joint_id(self):
        """
        Returns the id of the active joint
        """
        return self.current_joint["id"]
    
    def get_name(self):
        return self.config["name"]
    
    def make_joint_command(self, rel_velocity):
        """
        Builds a joint command from a relative velocity.
        Velocities used for groups are given between -1 and 1
        and must be scaled by the min and max speeds defined in the config
        """
        assert abs(rel_velocity) <= 1
        joint = self.current_joint
        max_v = joint["max_speed"] if "max_speed" in joint else 1.0
        min_v = joint["min_speed"] if "min_speed" in joint else 0.0
        velocity = 0.0
        if abs(rel_velocity) > 0:
            velocity = abs(rel_velocity)*(max_v - min_v) + min_v
            velocity *= math.copysign(1, rel_velocity)
        
        return bento_controller.msg.JointCommand(type = "velocity", velocity = velocity, id = self.get_current_joint_id())           