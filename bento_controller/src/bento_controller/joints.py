#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState
from dynamixel_controllers.srv import SetSpeed
from dynamixel_controllers.srv import TorqueEnable as TE
import math

class Joint(object):
  def __init__(self, joint_id, joint_def, joint_state):
    self.joint_id = joint_id
    self.joint_def = joint_def
    
    # Do not set again, this is a pointer to a buffer object
    # write the values of the joint_state
    self.joint_state = joint_state
    self.joint_state.joint_id = joint_id
    self.torque_enabled = False
  def handle_velocity_command(self, command):
    raise NotImplementedError
  def handle_position_command(self, command):
    raise NotImplementedError
  def handle_position_and_velocity_command(self, command):
    raise NotImplementedError
  def handle_torque_command(self, command):
    raise NotImplementedError
  def get_joint_id(self):
    return self.joint_id
  def get_joint_name(self):
    return self.joint_def["joint_name"]
  def get_joint_state(self):
    return self.joint_state
  def get_nice_name(self):
    return self.joint_def["nice_name"]
  def get_rviz_name(self):
    return self.joint_def.get("rviz_name", self.get_joint_name())
  def go_home(self, speed):
    raise NotImplementedError
  def torque_enable(self, enable):
    raise NotImplementedError
  def stop(self):
    raise NotImplementedError

class DynamixelJoint(Joint):
  def __init__(self, joint_id, joint_def, joint_state, controller_name):
    """
    Create services and publishers for the controller corresponding to the joint and type.
    Stores these all in self.joints for later use.
    """      
    Joint.__init__(self, joint_id, joint_def, joint_state)
    self.velocity_pub = rospy.Publisher('/%s/command/velocity' % controller_name, Float64, queue_size=1)
    self.position_pub = rospy.Publisher('/%s/command/position' % controller_name, Float64, queue_size=1)
    self.torque_pub = rospy.Publisher('/%s/command/torque' % controller_name, Float64, queue_size=1)
    self.speed_service = rospy.ServiceProxy(controller_name + '/set_speed', SetSpeed)
    self.torque_enable_service = rospy.ServiceProxy(controller_name + '/torque_enable', TE)
    
    self.joint_state_sub = rospy.Subscriber('/%s/state' % controller_name, JointState, self.state_update)
    
    self.joint_def = joint_def
    self.torque_enabled = True
    
  def _can_control(self):
    return self.torque_enabled
    
  def close(self):
    self.position_pub.unregister()
    self.velocity_pub.unregister()
    self.torque_pub.unregister()
    self.speed_service.close()
    self.torque_enable_service.close()
    self.joint_state_sub.unregister()
    
  def state_update(self, msg):
    # copy over elements, do not set joint_state directly
    self.joint_state.header = msg.header
    self.joint_state.name = msg.name
    self.joint_state.motor_ids = msg.motor_ids
    self.joint_state.motor_temps = msg.motor_temps
    self.joint_state.goal_pos = msg.goal_pos
    self.joint_state.current_pos = msg.current_pos
    self.joint_state.error = msg.error
    self.joint_state.velocity = msg.velocity
    self.joint_state.load = msg.load
    self.joint_state.is_moving = msg.is_moving
    self.joint_state.torque_enabled = self.torque_enabled
    
  def get_home_position(self):
    return self.joint_def["home"] if "home" in self.joint_def else math.pi
    
  def go_home(self, speed):
    self.speed_service(speed = speed)    
    self.position_pub.publish(self.get_home_position())
    self.torque_enabled = True
    
  def handle_velocity_command(self, command):
    """
    Handles a velocity command. Publishes a command to the velocity controller for the  joint
    """
    if self._can_control():
      self.velocity_pub.publish(command.velocity)
    
  def handle_position_command(self, command):
    """
    Handles a position command. Publishes a command to the position controller for the joint
    """
    if self._can_control():
      self.position_pub.publish(command.position)
    
  def handle_torque_command(self, command):
    if self._can_control():
      self.torque_pub.publish(self, command.torque)
    
  def handle_position_and_velocity_command(self, command):
    """
    Handles a position and velocity command. Sets the speed for the joint and then publishes a position
    command for the joint. Not sure if it's working at present.
    """
    if self._can_control():
      # TODO: this one does not appear to work at the moment
      # TODO: this is probably not how we should do this
      self.speed_service.call(abs(command.velocity))
      self.position_pub.publish(command.position)
    
  def stop(self):
    self.velocity_pub.publish(0.0)
    
  def torque_enable(self, enable):
    if not enable:
      self.speed_service.call(0)
    self.torque_enable_service(enable)
    self.torque_enabled = enable