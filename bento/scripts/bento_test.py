import rospy
from std_msgs.msg import String
from bento_controller.msg import BentoCommand
from bento_controller.msg import JointCommand

if __name__ == '__main__':    
    rospy.init_node('bento_test')
    pub = rospy.Publisher('/bento/command', BentoCommand)
    velocity = range(-3, 4)
    ids = range(1,5)
  
    # TODO: need to explicitly set velocities before calling this
    pub.publish(BentoCommand([JointCommand(type=String('position'), id=1, position = 3), 
                              JointCommand(type=String('position'), id=2, position = 3), 
                              JointCommand(type=String('position'), id=3, position = 3), 
                              JointCommand(type=String('position'), id=4, position = 3)]))
    rospy.sleep(2)
    
    commands = [BentoCommand([JointCommand(type=String('velocity'), id=i, velocity=v) for i in ids]) for v in velocity]
    
    while not rospy.is_shutdown():
        for command in commands:
            pub.publish(command)
            rospy.sleep(0.5)