#!/bin/bash
roscore_id=`ps -fu $USER | grep "roscore" | grep -v "grep" | awk '{print $2}'`
echo $roscore_id
if [ -z "${roscore_id}" ]
then
    roscore &
    sleep 1
else
    echo "roscore already running"
fi

roslaunch bento all.launch

